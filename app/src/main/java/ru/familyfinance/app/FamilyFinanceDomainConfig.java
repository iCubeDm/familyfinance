package ru.familyfinance.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.familyfinance.domain.api.commerzbank.csv.CsvParser;
import ru.familyfinance.domain.api.commerzbank.csv.ParserFactory;
import ru.familyfinance.domain.category.CategoryService;
import ru.familyfinance.domain.repository.CategoryRepository;
import ru.familyfinance.domain.repository.TransactionRepository;
import ru.familyfinance.domain.transaction.TransactionService;

/**
 * author: dmitry.yakubovsky
 * date:   17/03/16
 */
@Configuration
public class FamilyFinanceDomainConfig {

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private ParserFactory parserFactory;


    @Bean
    public TransactionService transactionService() {
        return new TransactionService(transactionRepository, parserFactory, categoryRepository);
    }

    @Bean
    public CategoryService categoryService() {
        return new CategoryService(categoryRepository, transactionRepository);
    }
}

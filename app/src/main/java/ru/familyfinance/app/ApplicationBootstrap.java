package ru.familyfinance.app;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.familyfinance.domain.DomainService;
import ru.familyfinance.domain.category.CategoryService;
import ru.familyfinance.domain.transaction.TransactionService;
import ru.familyfinance.integration.IntegrationConfig;
import ru.familyfinance.persistence.PersistenceConfig;
import ru.familyfinance.rest.RestInitializer;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/**
 * author: dmitry.yakubovsky
 * date:   18/02/16
 */
public class ApplicationBootstrap {

    private final static Logger logger = LoggerFactory.getLogger(ApplicationBootstrap.class);

    private static final Map<Class, DomainService> services = new HashMap<>();

    public static void main(String[] args) {

        printApplicationAsciiBanner("banner.txt");

        final ApplicationContext context = createSpringContext();

        services.put(TransactionService.class, context.getBean(TransactionService.class));
        services.put(CategoryService.class, context.getBean(CategoryService.class));

        RestInitializer.initialize(services);

    }

    private static ApplicationContext createSpringContext() {

        return new AnnotationConfigApplicationContext(
                FamilyFinanceDomainConfig.class,
                PersistenceConfig.class,
                IntegrationConfig.class
        );
    }

    private static void printApplicationAsciiBanner(final String fileName) {

        try {

            InputStream inputStream = ApplicationBootstrap.class.getClassLoader().getResourceAsStream(fileName);
            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = br.readLine()) != null) {
                System.out.println(line);
            }

        } catch (Exception e) {

            logger.warn("Could not display application ascii banner.", e);
        }
    }
}

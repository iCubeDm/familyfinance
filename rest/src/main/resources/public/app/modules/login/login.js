(function (angular) {
    angular
        .module('login', [])
        .config(['$stateProvider', function LoginConfig($stateProvider) {
            $stateProvider
                .state('login', {
                    url: "/login",
                    templateUrl: "app/modules/login/templates/login.tpl.html",
                    controller: "LoginCtrl"
                })
        }])
    ;
})(angular);
(function (angular) {

    function LoginController(auth, $state) {

        var self = this;

        if (auth.isLoggedIn()) {
            $state.reload();
        }

        self.credentials = {
            login: "",
            password: ""
        };

        self.submit = function submit() {
            auth.logIn(self.credentials);
        };
    }

    LoginController.$inject = ['auth', '$state'];
    angular
        .module('login')
        .controller('LoginCtrl', LoginController)
    ;


})(angular);

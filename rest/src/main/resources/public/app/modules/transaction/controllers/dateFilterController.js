(function (angular) {

    function DateFilterController(transactionService) {

        var dfCtrl = this;

        dfCtrl.filterDate = transactionService.getDefaultFilter();

        const DAYS10 = '10 days';
        const MONTH = '1 month';
        const MONTHS3 = '3 months';
        const MONTHS6 = '6 months';

        dfCtrl.chosenPeriod = MONTH;
        dfCtrl.periods = [
            DAYS10,
            MONTH,
            MONTHS3,
            MONTHS6
        ];

        dfCtrl.setPeriod = function setPeriod() {
            switch (dfCtrl.chosenPeriod) {
                case DAYS10:
                    dfCtrl.last10Days();
                    break;
                case MONTH:
                    dfCtrl.lastMonth();
                    break;
                case MONTHS3:
                    dfCtrl.last3Months();
                    break;
                case MONTHS6:
                    dfCtrl.last6Months();
                    break;
                default:
                    dfCtrl.lastMonth();
            }
        };

        dfCtrl.plusMonth = function plusMonth() {
            dfCtrl.filterDate.fromDate = new Date(new Date(dfCtrl.filterDate.fromDate).setMonth(dfCtrl.filterDate.fromDate.getMonth() + 1));
            dfCtrl.filterDate.toDate = new Date(new Date(dfCtrl.filterDate.toDate).setMonth(dfCtrl.filterDate.toDate.getMonth() + 1));

            transactionService.setFilter(dfCtrl.filterDate);
        };

        dfCtrl.minusMonth = function minusMonth() {
            dfCtrl.filterDate.fromDate = new Date(new Date(dfCtrl.filterDate.fromDate).setMonth(dfCtrl.filterDate.fromDate.getMonth() - 1));
            dfCtrl.filterDate.toDate = new Date(new Date(dfCtrl.filterDate.toDate).setMonth(dfCtrl.filterDate.toDate.getMonth() - 1));

            transactionService.setFilter(dfCtrl.filterDate);
        };


        dfCtrl.lastMonth = function lastMonth() {
            dfCtrl.filterDate = transactionService.getDefaultFilter();

            transactionService.setFilter(dfCtrl.filterDate);
        };

        dfCtrl.last3Months = function last3Months() {
            dfCtrl.filterDate.toDate = new Date();
            dfCtrl.filterDate.fromDate = new Date(new Date(dfCtrl.filterDate.toDate).setMonth(dfCtrl.filterDate.toDate.getMonth() - 3));

            transactionService.setFilter(dfCtrl.filterDate);
        };

        dfCtrl.last6Months = function last6Months() {
            dfCtrl.filterDate.toDate = new Date();
            dfCtrl.filterDate.fromDate = new Date(new Date(dfCtrl.filterDate.toDate).setMonth(dfCtrl.filterDate.toDate.getMonth() - 6));

            transactionService.setFilter(dfCtrl.filterDate);
        };

        dfCtrl.last10Days = function last10Days() {
            dfCtrl.filterDate.toDate = new Date();
            dfCtrl.filterDate.fromDate = new Date(new Date(dfCtrl.filterDate.toDate).setDate(dfCtrl.filterDate.toDate.getDate() - 10));

            transactionService.setFilter(dfCtrl.filterDate);
        };
    }

    DateFilterController.$inject = ['transactionService'];

    angular.module('transaction')
        .controller('DateFilterController', DateFilterController);


})(angular);

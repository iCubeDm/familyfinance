(function (angular) {

    function NewTransactionController($scope, transactionService) {

        var newTxCtrl = this;

        newTxCtrl.newTransaction = {
            "amount": 0,
            "executionDate": new Date(),
            "description": "",
            "categoryId": 0
        };

        $scope.init = function init(categories) {
            newTxCtrl.categories = categories;
        };

        newTxCtrl.save = function save() {
            var txForSave = newTxCtrl.newTransaction;
            //category = _.findWhere(newTxCtrl.categories, {id: txForSave.categoryId*1});
            //txForSave.categoryId = category.id;
            transactionService.saveTransaction(txForSave)
                .then(function success(data) {
                        newTxCtrl.newTransaction = {
                            "amount": 0,
                            "executionDate": new Date(),
                            "description": "",
                            "category": 0
                        };
                        $scope.$parent.txCtrl.isNewRequested = false;
                        $scope.$parent.txCtrl.loadTransactions();
                    },
                    function error(callback) {

                        newTxCtrl.newTransaction = {
                            "amount": 0,
                            "executionDate": new Date(),
                            "description": "",
                            "category": 0
                        };
                        $scope.$parent.txCtrl.isNewRequested = false;

                        if (callback) {
                            callback();
                        }
                    });
        };

        newTxCtrl.toggleEdit = function () {
            $scope.$parent.txCtrl.toggleNewTransaction();
        }

    }

    NewTransactionController.$inject = ['$scope', 'transactionService'];

    angular.module('transaction')
        .controller('NewTransactionController', NewTransactionController);


})(angular);
(function (angular) {

    function TransactionRowController($scope, transactionService, $state) {
        var rowCtrl = this;
        rowCtrl.isLongDescription = false;
        rowCtrl.isEdit = false;

        $scope.init = function init(transaction, categories) {
            rowCtrl.transaction = transaction;
            rowCtrl.categories = categories;
        };

        rowCtrl.switchShow = function switchShow() {
            rowCtrl.isLongDescription = !rowCtrl.isLongDescription;
        };

        rowCtrl.toggleEdit = function toggleEdit() {
            rowCtrl.isEdit = !rowCtrl.isEdit;
        };

        rowCtrl.getCategoryName = function getCategoryName() {
            var category = _.findWhere(rowCtrl.categories, {id: rowCtrl.transaction.categoryId * 1});
            return category ? category.name : "-";
        };

        rowCtrl.saveTransaction = function saveTransaction() {
            transactionService.saveTransaction(rowCtrl.transaction).then(
                function success(data) {
                    rowCtrl.transaction.categoryApproved = true;
                    $scope.$parent.txCtrl.loadTransactions();
                    rowCtrl.isEdit = false;
                },
                function error(callback) {
                    callback();
                }
            );
        };

        rowCtrl.deleteTransaction = function deleteTransaction() {
            transactionService.deleteTransaction(rowCtrl.transaction)
                .then(
                    function success(data) {
                        $scope.$parent.txCtrl.loadTransactions();
                    }, function fail(callback) {
                        callback();
                    }
                )
        };

        rowCtrl.approveCategory = function approveCategory() {
            rowCtrl.transaction.categoryApproved = true;
            transactionService.saveTransaction(rowCtrl.transaction).then(
                function success(data) {
                    rowCtrl.isEdit = false;
                },
                function error(callback) {
                    callback();
                }
            );
        };

        rowCtrl.clearCategory = function clearCategory() {
            transactionService.clearCategory(rowCtrl.transaction).then(
                function success(data) {
                    rowCtrl.transaction.categoryId = 0;
                    rowCtrl.isEdit = false;
                },
                function error(callback) {
                    callback();
                }
            );
        };
    }

    TransactionRowController.$inject = ['$scope', 'transactionService', '$state'];

    angular.module('transaction')
        .controller('TransactionRowController', TransactionRowController);

})(angular);
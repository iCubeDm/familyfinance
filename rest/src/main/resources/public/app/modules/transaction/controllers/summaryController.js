(function (angular) {

    function SummaryController($scope) {
        var summaryCtrl = this;

        if(!$scope.filteredTransactions){
            $scope.filteredTransactions = [];
        }

        summaryCtrl.revenues = function revenues() {
            var sum = 0;
            $scope.filteredTransactions.forEach(function forEachFiltered(tx) {
                if (tx.amount * 1 > 0)
                    sum += tx.amount * 1;
            });

            return sum;
        };

        summaryCtrl.expenses = function expenses() {
            var sum = 0;
            $scope.filteredTransactions.forEach(function forEachFiltered(tx) {
                if (tx.amount * 1 < 0)
                    sum += tx.amount * 1;
            });

            return sum;
        };

        summaryCtrl.balance = function balance() {
            var sum = 0;
            $scope.filteredTransactions.forEach(function forEachFiltered(tx) {
                sum += tx.amount * 1;
            });

            return sum;
        };
    }

    SummaryController.$inject = ['$scope'];

    angular.module('transaction')
        .controller('SummaryController', SummaryController);

})(angular);
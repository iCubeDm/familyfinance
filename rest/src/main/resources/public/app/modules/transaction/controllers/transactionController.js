(function (angular) {

    function TransactionController($scope, transactionService, categoryService, $state, $timeout) {
        var txCtrl = this;

        $scope.isLoading = true;

        txCtrl.transactions = [];
        txCtrl.categories = [];
        txCtrl.isNewRequested = false;
        txCtrl.sortType = 'executionDate'; // значение сортировки по умолчанию
        txCtrl.sortReverse = true;  // обратная сортировка
        txCtrl.searchTransaction = {};     // значение поиска по умолчанию
        txCtrl.templates = {
            transactionRow: './app/modules/transaction/templates/partials/transaction-row.tpl.html',
            searchFilter: './app/modules/transaction/templates/partials/search-filter.tpl.html',
            newTransaction: './app/modules/transaction/templates/partials/new-transaction.tpl.html',
            summary: './app/modules/transaction/templates/partials/summary.tpl.html',
            dateFilter: './app/modules/transaction/templates/partials/date-filter.tpl.html'
        };

        txCtrl.toggleNewTransaction = function toggleNewTransaction() {
            txCtrl.isNewRequested = !txCtrl.isNewRequested;
        };

        txCtrl.toggleCategoryApproved = function toggleCategoryApproved() {
            if (txCtrl.searchTransaction) {
                txCtrl.searchTransaction.categoryApproved = !txCtrl.searchTransaction.categoryApproved;
            }
        };

        txCtrl.resetFilter = function resetFilter() {
            txCtrl.searchTransaction = {};
        };

        txCtrl.refresh = function refresh() {
            $state.reload();
        };


        txCtrl.loadTransactions = function loadTransactions() {
            txCtrl.loadCategories();
            transactionService.loadTransactionsForPeriod()
                .then(function success(response) {

                    txCtrl.transactions = response;
                    txCtrl.transactions.forEach(function (tx) {
                        tx.amount = tx.amount * 1;
                        tx.executionDate = new Date(tx.executionDate);
                    });

                }, function error(callback) {
                    callback();
                });
        };

        txCtrl.approveAll = function approveAll() {
            var transactionsForSave = [];

            $scope.filteredTransactions.forEach(function (tx) {
                if (tx.categoryId != 0 && tx.categoryId != "0" && tx.categoryApproved == false) {
                    tx.categoryApproved = true;
                    transactionsForSave.push(tx);
                }
            });

            transactionService.saveTransactions(transactionsForSave).then(
                function success(data) {
                    txCtrl.loadTransactions();
                },
                function (callback) {
                    $scope.filteredTransactions.forEach(function (tx) {
                        tx.categoryApproved = false
                    });
                    callback();
                }
            );
        };

        txCtrl.loadCategories = function loadCategories() {
            categoryService.loadCategories().then(
                function success(data) {
                    txCtrl.categories = data;
                },
                function error(callback) {
                    callback();
                }
            );
        };
        txCtrl.loadTransactions();

        txCtrl.setIsLoading = function (value) {
            $scope.isLoading = value;
        };
    }

    TransactionController.$inject = ['$scope', 'transactionService', 'categoryService', '$state', '$timeout'];

    angular.module('transaction')
        .controller('TransactionController', TransactionController)
        .directive('repeatDone', function RepeatDone() {
            return function (scope, element, attrs) {
                if (scope.$last) {
                    scope.$eval(attrs.repeatDone);
                }
            }
        })
        .directive('repeatStart', function RepeatStart() {
            return function (scope, element, attrs) {
                if (scope.$first) {
                    scope.$eval(attrs.repeatStart)
                }
            }
        })
        .config(['$stateProvider', function TransactionConfig($stateProvider) {
            $stateProvider
                .state('main.transaction', {
                    url: '/transaction',
                    templateUrl: "app/modules/transaction/templates/transactions.tpl.html"
                })
        }]);

})(angular);

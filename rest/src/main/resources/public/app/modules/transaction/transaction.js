(function (angular) {
    angular
        .module('transaction', [])
        .config(function transactionConfig($mdDateLocaleProvider) {
            $mdDateLocaleProvider.formatDate = function (date) {
                return $.format.date(new Date(date), 'dd.MM.yyyy')
            };
        });
})(angular);
(function (angular) {

    angular.module('main', [])
        .config(['$stateProvider', function MainConfig($stateProvider) {
            $stateProvider
                .state('main', {
                    url: '/main',
                    templateUrl: "app/modules/main/templates/main.tpl.html",
                    controller: "MainCtrl"
                })
        }]);

})(angular);
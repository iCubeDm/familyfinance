(function (angular) {

    function MainController($scope, session, auth, $location) {

        var self = this;

        $scope.user = session.getUser();

        self.logout = function logout() {
            auth.logOut();
        };

        $scope.$watch('user', function (user) {
            if (user == null) {
                $location.path('/login');
            }
        })
    }

    MainController.$inject = ['$scope', 'session', 'auth', '$location'];

    angular
        .module('main')
        .controller('MainCtrl', MainController)
    ;


})(angular);
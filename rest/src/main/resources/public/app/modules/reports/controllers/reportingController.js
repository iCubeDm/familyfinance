(function (angular) {
    "use strict";

    function ReportingController($scope, categoryService) {
        var ctrl = this;

        ctrl.options = {};
        ctrl.dataExpenses = [];
        ctrl.dataRevenues = [];
        ctrl.isBar = true;
        ctrl.report = {
            expenses: [],
            revenues: []
        };
        ctrl.filterDate = categoryService.getDefaultFilter();

        initBar();

        ctrl.loadReportForPeriod = function loadReportForPeriod() {
            initData();
            if (ctrl.isBar) initBar();
            else initCircle();
        };

        $scope.$watch('ctrl.dataExpenses', function (dataExpenses) {
            if (ctrl.isBar) initBar();
            else initCircle();
        });

        function initData() {
            categoryService.loadReport(ctrl.filterDate).then(
                function success(data) {
                    ctrl.dataRevenues = data.revenues;
                    ctrl.dataExpenses = data.expenses;
                },
                function error(callback) {
                    callback();
                }
            );
        }

        function initCircle() {

            ctrl.report = {
                expenses: [],
                revenues: []
            };
            ctrl.options = getPieChartOptions();

            ctrl.dataRevenues.forEach(function (pair) {
                ctrl.report.revenues.push({
                    label: pair.categoryName,
                    value: pair.sumAmount * 1
                });
            });
            ctrl.dataExpenses.forEach(function (pair) {
                ctrl.report.expenses.push({
                    label: pair.categoryName,
                    value: -1 * (pair.sumAmount * 1)
                });
            });
        }

        function initBar() {
            ctrl.report = {
                expenses: [],
                revenues: []
            };
            ctrl.options = getDescreetBarChartOptions();
            ctrl.report.expenses = [{
                key: "Cumulative Return",
                values: []
            }];
            ctrl.report.revenues = [{
                key: "Cumulative Return",
                values: []
            }];

            ctrl.dataRevenues.forEach(function (pair) {
                ctrl.report.revenues[0].values.push({
                    label: pair.categoryName,
                    value: pair.sumAmount * 1
                });
            });
            ctrl.dataExpenses.forEach(function (pair) {
                ctrl.report.expenses[0].values.push({
                    label: pair.categoryName,
                    value: -(pair.sumAmount * 1)
                });
            });
        }

        ctrl.changeChartType = function () {
            ctrl.isBar = !ctrl.isBar;
            if (ctrl.isBar) initBar();
            else initCircle();
        };


        function getPieChartOptions() {
            return {
                chart: {
                    type: 'pieChart',
                    height: 500,
                    x: function (d) {
                        return d.label;
                    },
                    y: function (d) {
                        return d.value;
                    },
                    showLabels: true,
                    duration: 500,
                    labelThreshold: 0.001,
                    labelSunbeamLayout: true,
                    legend: {
                        margin: {
                            top: 5,
                            right: 35,
                            bottom: 5,
                            left: 0
                        }
                    }
                }
            };
        }

        function getDescreetBarChartOptions() {
            return {
                chart: {
                    type: 'discreteBarChart',
                    height: 450,
                    margin: {
                        top: 20,
                        right: 20,
                        bottom: 50,
                        left: 55
                    },
                    x: function (d) {
                        return d.label;
                    },
                    y: function (d) {
                        return d.value + (1e-10);
                    },
                    showValues: true,
                    valueFormat: function (d) {
                        return d3.format(',.2f')(d);
                    },
                    duration: 500,
                    xAxis: {},
                    yAxis: {
                        axisLabel: '',
                        axisLabelDistance: -1000
                    }
                }
            };
        }

    }

    ReportingController.$inject = ['$scope', 'categoryService'];

    angular.module('reports')
        .controller('ReportingController', ReportingController)
        .config(['$stateProvider', function CategoryConfig($stateProvider) {
            $stateProvider
                .state('main.reports', {
                    url: '/reports',
                    templateUrl: "./app/modules/reports/templates/reports.tpl.html"
                })
        }]);

})(angular);
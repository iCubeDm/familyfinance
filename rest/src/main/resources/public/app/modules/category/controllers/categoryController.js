(function (angular) {

    function CategoryController($scope, categoryService, transactionService) {

        var ctrl = this;

        ctrl.isIdentificating = false;
        ctrl.categories = [];
        ctrl.templates = {
            editCategory: "./app/modules/category/templates/edit-category.tpl.html"
        };

        ctrl.loadCategories = function loadCategories() {
            categoryService.loadCategories().then(
                function success(data) {
                    ctrl.categories = data;
                });
        };
        ctrl.loadCategories();


        ctrl.runIdentification = function runIdentification() {
            ctrl.isIdentificating = true;
            transactionService.identifyCategories()
                .then(
                    function success() {
                        ctrl.loadCategories();
                        ctrl.isIdentificating = false;
                    },
                    function error(callback) {
                        callback();
                        ctrl.isIdentificating = false;
                    });
        };

        ctrl.newCategory = {
            name: ""
        };

        ctrl.sortType = 'name'; // значение сортировки по умолчанию
        ctrl.sortReverse = true;  // обратная сортировка
        ctrl.searchCategory = '';     // значение поиска по умолчанию


        ctrl.addNewCategory = function addNewCategory() {

            var categoryForSave = ctrl.newCategory;

            categoryForSave.name = categoryForSave.name.trim();

            if (categoryForSave.name.length > 0) {
                categoryService.saveCategory(categoryForSave)
                    .then(function () {
                        ctrl.loadCategories();
                    });
            }

            ctrl.newCategory = {name: ""};
        };
    }

    CategoryController.$inject = ['$scope', 'categoryService', 'transactionService'];

    angular.module('category')
        .controller('CategoryController', CategoryController)
        .config(['$stateProvider', function CategoryConfig($stateProvider) {
            $stateProvider
                .state('main.category', {
                    url: '/category',
                    templateUrl: "./app/modules/category/templates/categories.tpl.html"
                })
        }]);
})(angular);
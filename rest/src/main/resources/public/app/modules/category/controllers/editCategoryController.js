(function (angular) {
    "use strict";
    angular.module('category')
        .controller('EditCategoryController', EditCategoryController);

    EditCategoryController.$inject = ['$scope', 'categoryService'];

    function EditCategoryController($scope, categoryService) {
        console.log('editctrl');
        var ctrl = this;
        ctrl.isEdit = false;
        ctrl.category = $scope.category;

        ctrl.toggleEdit = function toggleEdit() {
            ctrl.isEdit = !ctrl.isEdit;
        };

        ctrl.deleteCategory = function deleteCategory() {
            categoryService.deleteCategory(ctrl.category.id)
                .then(function success() {
                    $scope.$parent.ctrl.loadCategories();
                })
        };

        ctrl.saveCategory = function saveCategory() {
            ctrl.category.name = ctrl.category.name.trim();
            categoryService.saveCategory(ctrl.category)
                .then(function success() {
                    console.log('saved');
                    ctrl.isEdit = false;
                    $scope.$parent.ctrl.loadCategories();
                }, function error(callback) {
                    callback();
                });
        };

    }


})(angular);
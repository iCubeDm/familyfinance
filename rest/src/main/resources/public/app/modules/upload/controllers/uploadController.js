(function (angular) {

    function UploadController($scope, FileUploader, session) {
        var ctrl = this;
        ctrl.banksAvailable = ['COMMERZBANK', 'NUMBER26'];

        $scope.uploader = new FileUploader({
            url: '/api/transactions/upload',
            headers: {
                "Authorization": "Bearer " + session.getToken(),
                "Bank": 'COMMERZBANK'
            }
        });

        console.log($scope.uploader);
    }

    UploadController.$inject = ['$scope', 'FileUploader', 'session'];

    angular.module('upload')
        .controller('UploadController', UploadController)
        .config(['$stateProvider', function UploadConfig($stateProvider) {
            $stateProvider
                .state('main.upload', {
                    url: "/upload",
                    templateUrl: "app/modules/upload/templates/upload.tpl.html"
                })
        }]);

})(angular);

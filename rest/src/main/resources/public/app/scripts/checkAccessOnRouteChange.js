(function (angular) {

    function checkAccessOnStateChange($rootScope, auth, $location) {

        // Listen for location changes
        // This happens before route or state changes
        $rootScope.$on('$locationChangeStart', function (event, newUrl, oldUrl) {
            if (!auth.isLoggedIn()) {
                $location.path('/login');
                // Prevent location change
            }
        });

        // Listen for route changes when using ngRoute
        $rootScope.$on('$routeChangeStart', function (event, nextRoute, currentRoute) {
            // Here we simply check if logged in but you can
            // implement more complex logic that inspects the
            // route to see if access is allowed or not
            if (!auth.isLoggedIn()) {
                $location.path('/login');
                // Prevent state change
            }
        });

        // Listen for state changes when using ui-router
        $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
            // Here we simply check if logged in but you can
            // implement more complex logic that inspects the
            // state to see if access is allowed or not
            if (!auth.isLoggedIn()) {
                $location.path('/login');
                // Prevent state change
            }
        });
    }

    // Inject dependencies
    checkAccessOnStateChange.$inject = ['$rootScope', 'auth', '$location'];

    // Export
    angular
        .module('app')
        .run(checkAccessOnStateChange);

})(angular);

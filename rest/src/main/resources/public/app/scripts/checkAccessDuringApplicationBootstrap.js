(function (angular) {

    function checkAccessDuringApplicationBootstrap($location, auth) {

        if (!auth.isLoggedIn()) {
            // Redirect to third party login page
            $location.path('/login');
        }

    }

    // Inject dependencies
    checkAccessDuringApplicationBootstrap.$inject = ['$location', 'auth'];

    // Export
    angular
        .module('app')
        .run(checkAccessDuringApplicationBootstrap);

})(angular);

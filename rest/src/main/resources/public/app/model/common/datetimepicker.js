(function (angular) {

    angular.module('app')
        .directive('datetimez', function DatePickerDirective() {
            return {
                restrict: 'A',
                require: 'ngModel',
                templateUrl: 'app/model/common/templates/datepicker.tpl.html',
                link: function compileFunction(scope, element, attrs, ngModelCtrl) {
                    element.datetimepicker({
                        dateFormat: 'dd.MM.yyyy'
                    }).on('changeDate', function (e) {
                        ngModelCtrl.$setViewValue(e.date);
                        scope.$apply();
                    });
                }
            };
        });
})(angular);
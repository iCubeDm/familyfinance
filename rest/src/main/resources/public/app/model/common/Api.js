var Model = Model || {};

Model.Base = function (options) {
    this.isLoading = false;
    this.options = options;
    this.body = [];
};

Model.Base.prototype.request = function request(config) {

    var $deferred = $.Deferred(),
        that = this;

    this.isLoading = true;

    config.success = function success(data) {
        that.body = data;
        that.isLoading = false;
        $deferred.resolve(that);
    };

    config.error = function fail(data) {
        console.log("Request failed (api): ", data);
        that.isLoading = false;
        $deferred.reject(data.status);
    };

    $.ajax(config.url, config);

    return $deferred.promise();
};


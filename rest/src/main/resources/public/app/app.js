(function (angular) {
    'use strict';
    angular.module('app', [
            "ui.router",
            'ngMaterial',
            'angularFileUpload',
            'nvd3',
            'main',
            'transaction',
            'category',
            'login',
            'upload',
            'reports'
        ])
        .config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
            $urlRouterProvider.otherwise("/main")
        }]);
})(angular);







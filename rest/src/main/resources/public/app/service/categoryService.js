(function (angular) {
    function CategoryService($http, $q, session,$location) {

        const SAVE = 'saveCategory';
        const DELETE = 'deleteCategory';
        var authorizationHeader = 'Bearer ' + session.getToken();

        var _doRequest = function doRequest(method, url, body) {

            var deferred = $q.defer();
            var config = {
                method: method,
                url: url,
                data: JSON.stringify(body),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': authorizationHeader
                }
            };

            $http(config).then(
                function success(response) {
                    deferred.resolve(response.data);
                },
                function error(response) {
                    if (response.status == 401) {
                        session.destroy();
                        $location.path('/login');
                        deferred.reject('401');
                    }
                }
            );

            return deferred.promise;
        };

        this.saveCategory = function saveCategory(category) {
            return _doRequest('POST', '/api/category', category);
        };

        this.deleteCategory = function deleteCategory(categoryId) {

            return _doRequest('DELETE', '/api/category/' + categoryId, "");
        };

        this.loadCategories = function loadCategories() {

            return _doRequest('GET', '/api/categories');
        };

        this.loadReport = function loadReport(filterDate) {
            if (!filterDate) {
                filterDate = this.getDefaultFilter();
            }

            var fromDateStr = new Date(filterDate.fromDate).toISOString();
            var toDateStr = new Date(filterDate.toDate).toISOString();

            return _doRequest('GET', '/api/categories/report/' + fromDateStr + "/" + toDateStr);
        };

        this.getDefaultFilter = function getDefaultFilter() {
            filter = {
                fromDate: new Date(),
                toDate: new Date()
            };
            filter.fromDate.setDate(filter.fromDate.getDate() - 30);
            filter.toDate.setDate(filter.toDate.getDate() + 1);
            filter.fromDate = new Date(filter.fromDate.getTime());
            filter.toDate = new Date(filter.toDate.getTime());

            return filter;
        };
    }


// Inject dependencies
    CategoryService.$inject = ['$http', '$q', 'session','$location'];

// Export
    angular
        .module('app')
        .service('categoryService', CategoryService);

})
(angular);
(function (angular) {

    function AuthService($http, $location, session) {

        /**
         * Check whether the user is logged in
         * @returns boolean
         */
        this.isLoggedIn = function isLoggedIn() {
            const token = session.getToken();
            return token !== null && token !== "null";
        };

        /**
         * Log in
         *
         * @param credentials
         * @returns {*|Promise}
         */
        this.logIn = function (credentials) {
            const authorization = "Basic " + window.btoa(credentials.login + ":" + credentials.password);
            return $http({
                url: "/auth/token",
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": authorization
                }
            }).then(
                function (response) {
                    var data = response.data;
                    session.setUser(credentials.login);
                    session.setToken(data.token);
                    $location.path('/main');
                },
                function (response) {
                    session.destroy();
                    $location.path('/login');
                });
        };

        /**
         * Log out
         *
         * @returns {*|Promise}
         */
        this.logOut = function () {
            session.destroy();
            $location.path('/login');
        };

    }

    // Inject dependencies
    AuthService.$inject = ['$http', '$location', 'session'];

    // Export
    angular
        .module('app')
        .service('auth', AuthService);

})(angular);


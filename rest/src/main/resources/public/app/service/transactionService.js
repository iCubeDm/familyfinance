(function (angular) {

    function TransactionService($http, $q, session, $location) {

        var service = this;
        var authorizationHeader = 'Bearer ' + session.getToken();
        service.filter = {};

        this.getDefaultFilter = function getDefaultFilter() {
            service.filter = {
                fromDate: new Date(),
                toDate: new Date()
            };
            service.filter.fromDate.setDate(service.filter.fromDate.getDate() - 30);
            service.filter.toDate.setDate(service.filter.toDate.getDate() + 1);
            service.filter.fromDate = new Date(service.filter.fromDate.getTime());
            service.filter.toDate = new Date(service.filter.toDate.getTime());

            return service.filter;
        };
        this.getDefaultFilter();

        this.getFilter = function getFilter() {
            return service.filter;
        };

        this.setFilter = function setFilter(filterDate) {
            service.filter = filterDate;
        };

        this.saveTransaction = function saveTransaction(transaction) {
            transaction.executionDate = new Date(transaction.executionDate).toISOString();

            return _doRequest('POST', '/api/transaction', JSON.stringify(transaction));
        };

        this.clearCategory = function clearCategory(transaction) {

            transaction.executionDate = new Date(transaction.executionDate).toISOString();

            return _doRequest('POST', '/api/transaction/clear-category', JSON.stringify(transaction));
        };

        this.identifyCategories = function identifyCategories() {

            return _doRequest('GET', '/api/transactions/identify-categories', "");
        };

        this.saveTransactions = function saveTransactions(transactions) {
            transactions.forEach(function (tx) {
                tx.executionDate = new Date(tx.executionDate).toISOString();
            });

            return _doRequest('POST', '/api/transactions', JSON.stringify(transactions));
        };

        this.deleteTransaction = function deleteTransaction(transaction) {
            return _doRequest('DELETE', '/api/transaction/' + transaction.id);
        };

        this.loadTransactionsForPeriod = function loadTransactionsForPeriod() {

            var startDate = service.filter.fromDate.toISOString();
            var endDate = service.filter.toDate.toISOString();

            return _doRequest('GET', '/api/transactions/' + startDate + '/' + endDate, null);
        };


        var _doRequest = function doRequest(method, url, body) {

            var deferred = $q.defer();

            var config = {
                method: method,
                url: url,
                data: body,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': authorizationHeader
                }
            };

            $http(config).then(
                function success(response) {
                    deferred.resolve(response.data);
                },
                function error(response) {
                    if (response.status == 401) {
                        return function loginRedirect() {
                            session.destroy();
                            $location.path('/login');
                            deferred.reject('401');
                        }
                    }
                    else {
                        return function otherError() {
                            console.log('Error: ', data);
                        }
                    }
                }
            );

            return deferred.promise;

        };
    }

    TransactionService.$inject = ['$http', '$q', 'session', '$location'];

    angular.module('app').service('transactionService', TransactionService);

})(angular);

(function (angular) {

    function LocalStorageServiceFactory($window){
        if($window.localStorage){
            return $window.localStorage;
        }
        throw new Error('Local storage support is needed');
    }

    // Inject dependencies
    LocalStorageServiceFactory.$inject = ['$window'];

    // Export
    angular
        .module('app')
        .factory('localStorage', LocalStorageServiceFactory);

})(angular);
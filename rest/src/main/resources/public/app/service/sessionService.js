(function (angular) {

    function SessionService(localStorage) {

        // Instantiate data when service
        // is loaded
        this._user = localStorage.getItem('session.user');
        this._token = localStorage.getItem('session.token');

        this.getUser = function getUser() {
            return this._user;
        };

        this.setUser = function setUser(user) {
            this._user = user;
            localStorage.setItem('session.user', JSON.stringify(user));
            return this;
        };

        this.getToken = function getAccessToken() {
            return this._token;
        };

        this.setToken = function setAccessToken(token) {
            this._token = token;
            localStorage.setItem('session.token', token);
            return this;
        };

        /**
         * Destroy session
         */
        this.destroy = function destroy() {
            this.setUser(null);
            this.setToken(null);
        };

    }

    // Inject dependencies
    SessionService.$inject = ['localStorage'];

    // Export
    angular.module('app').service('session', SessionService);

})(angular);
package ru.familyfinance.rest;

import ru.familyfinance.domain.DomainService;
import ru.familyfinance.domain.category.CategoryService;
import ru.familyfinance.domain.transaction.TransactionService;
import ru.familyfinance.rest.resource.auth.AuthResource;
import ru.familyfinance.rest.resource.category.CategoryResource;
import ru.familyfinance.rest.resource.transaction.TransactionResource;

import java.util.Map;

import static ru.familyfinance.rest.filters.CustomFilters.afterFilters;
import static ru.familyfinance.rest.filters.CustomFilters.beforeFilters;
import static spark.Spark.*;

/**
 * Created by:
 * * dmitry.yakubovsky
 */
public class RestInitializer {

    public static void initialize(Map<Class, DomainService> services) {

        setupStaticFileLocation();

        port(8081);

        beforeFilters();

        ExceptionMapper.setup();

        TransactionService transactionService = (TransactionService) services.get(TransactionService.class);
        CategoryService categoryService = (CategoryService) services.get(CategoryService.class);

        setupEndpoints(transactionService, categoryService);

        afterFilters();

        awaitInitialization();

    }

    static void setupEndpoints(TransactionService transactionService, CategoryService categoryService) {

        // Routes --------------------------
        options("/*", (req, res) -> "{\"status\": \"OK\"}");

        get("/status", (req, res) -> "{\"status\": \"OK\"}");

        AuthResource.setupEndpoints();
        TransactionResource.setupEndpoints(transactionService);
        CategoryResource.setupEndpoints(categoryService);

        // ---------------------------------
    }

    static void setupStaticFileLocation() {
        staticFileLocation("/public");
    }
}

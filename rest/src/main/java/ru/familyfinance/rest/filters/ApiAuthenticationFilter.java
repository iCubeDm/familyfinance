package ru.familyfinance.rest.filters;

import com.google.common.hash.Hashing;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.familyfinance.domain.utils.DateTime;
import spark.Filter;
import spark.Request;
import spark.Response;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

import static spark.Spark.halt;

/**
 * author: dmitry.yakubovsky
 * date:   12/04/16
 */
public class ApiAuthenticationFilter implements Filter {

    private static final String login = "icubefamily";

    private static final String pass = "Olga0711";

    private static final String SALT = "SDLKFJHHSLD023408712=123-EW/DF";

    private final static Logger logger = LoggerFactory.getLogger(ApiAuthenticationFilter.class);

    @Override
    public void handle(Request request, Response response) throws Exception {

        logger.info("Requested url -> {}", request.uri());

        String authHeader = request.headers("Authorization");

        final boolean isBlank = StringUtils.isBlank(authHeader);
        final boolean isValidBearerFormat = authHeader.matches("Bearer \\S+");

        if (isBlank || !isValidBearerFormat) {
            logger.warn("Detected unauthorized request from -> {}", request.ip());
            halt(401);
        }

        String tokenString = new String(Base64.getDecoder().decode(authHeader.split(" ")[1]));

        if (!tokenString.matches("\\w+:\\w+")) {
            logger.warn("Detected unauthorized request from -> {}", request.ip());
            halt(401);
        }

        String token = tokenString.split(":")[0];
        String expired = tokenString.split(":")[1];

        final long currentMillis = DateTime.now().millis();
        final long expiredMillis = Long.parseLong(expired);
        boolean isExpired = currentMillis > expiredMillis;

        if (isExpired) {
            response.type("text/plain");
            halt(401, "Token Expired. Please relogin.");
        }

        final boolean isValidToken = isValidToken(token);

        if (!isValidToken) {
            logger.warn("Detected unauthorized request from -> {}", request.ip());
            halt(401);
        }

        logger.info("Request authenticated");
    }

    public static String generateUserToken(String login, String pass) {

        String str2hash = SALT + login + pass;
        return Hashing.sha256().hashString(str2hash, StandardCharsets.UTF_8).toString();
    }


    public static boolean isValidToken(String token) {
        return token.equals(generateUserToken(login, pass));
    }
}

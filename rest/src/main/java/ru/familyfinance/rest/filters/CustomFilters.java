package ru.familyfinance.rest.filters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import java.util.UUID;

import static spark.Spark.before;

public class CustomFilters {

    private static final Logger log = LoggerFactory.getLogger(CustomFilters.class);

    private static final String HEADER_TX_ID = "ff-tx-id";

    private static final String MDC_TX_KEY = "tx";

    public static void beforeFilters() {
//        final Config config = new AuthConfigFactory().build();
//        before("/*", new RequiresAuthenticationFilter(config, "IndirectBasicAuthClient"));
        auth();
        beforeTx();
        beforeLog();
        cors();
        contentType();
    }

    private static void auth() {
        before("/api/*", new ApiAuthenticationFilter());
    }

    public static void afterFilters() {
        afterCleanup();
    }

    private static void beforeTx() {

        before((req, res) -> {
            String txId = req.headers(HEADER_TX_ID);
            if (txId == null) {
                txId = UUID.randomUUID().toString();
            }
            res.header(HEADER_TX_ID, txId);
            MDC.put(MDC_TX_KEY, txId);
        });
    }

    private static void beforeLog() {

        before((req, res) -> {
            final String arg = req.pathInfo();
            if (arg.contains("bower_components") || arg.contains("/app") || arg.contains("/assets")) return;
            log.info("BEGIN - path={}", arg);
        });
    }

    private static void cors() {

        before((request, response) -> {
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
            response.header("Access-Control-Allow-Headers", "Content-Type, api_token, Authorization");
        });
    }

    private static void contentType() {

        before((request, response) -> {
            response.type("application/json");
        });
    }

    private static void afterCleanup() {

        before((req, res) -> {
            MDC.clear();
        });
    }

}

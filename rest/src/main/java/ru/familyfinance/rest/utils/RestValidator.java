package ru.familyfinance.rest.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.familyfinance.rest.exception.RequestValidationException;
import ru.familyfinance.rest.mediatype.MediaType;
import ru.familyfinance.rest.transformer.TransformerFactory;

import javax.validation.ConstraintViolation;
import javax.validation.Valid;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * author: dmitry.yakubovsky
 * date:   05/04/16
 */
public class RestValidator {

    private final static Logger logger = LoggerFactory.getLogger(RestValidator.class);

    private final Validator validator;

    List<Error> errors;

    public RestValidator() {
        super();
        this.validator = Validation.buildDefaultValidatorFactory().getValidator();
    }

    public void validate(Object target) throws RequestValidationException {
        errors = new ArrayList<>();

        Set<ConstraintViolation<Object>> constraintViolations = validator.validate(target);

        logger.info("Detected errors: {}", constraintViolations.size());
        if (constraintViolations.size() == 0) return;

        fillErrors(constraintViolations);

        throw new RequestValidationException(TransformerFactory.getInstance(MediaType.JSON).render(errors.toArray()));
    }

    public void validateBatch(List<Object> targets) throws RequestValidationException {
        errors = new ArrayList<>();

        final BatchedObject target = new BatchedObject(targets);

        Set<ConstraintViolation<Object>> constraintViolations = validator.validate(target);

        logger.info("Detected errors: {}", constraintViolations.size());
        if (constraintViolations.size() == 0) return;

        fillErrors(constraintViolations);

        throw new RequestValidationException(TransformerFactory.getInstance(MediaType.JSON).render(errors.toArray()));
    }

    private void fillErrors(Set<ConstraintViolation<Object>> constraintViolations) {
        for (ConstraintViolation<Object> cv : constraintViolations) {

            final String property = cv.getPropertyPath().toString();
            final String value = cv.getInvalidValue() == null ? null : cv.getInvalidValue().toString();
            final String message = cv.getMessage();

            errors.add(new Error(property, value, message));
        }
    }

    private static class BatchedObject {

        @Valid
        private List<Object> request;

        public BatchedObject(List<Object> targets) {
            this.request = targets;
        }
    }

    private static class Error {

        private String property;
        private String value;
        private String message;

        public Error(String property, String value, String message) {
            this.property = property;
            this.value = value;
            this.message = message;
        }
    }
}

package ru.familyfinance.rest.utils;


import ru.familyfinance.rest.mediatype.MediaType;

public class ErrorUtils {

    public static String createError(final String message, final MediaType resultType) {

        return new Error(message).toString();
    }

    private static final class Error {

        @SuppressWarnings("unused")
        private final String message;

        private Error(final String message) {
            super();
            if (message != null && (message.charAt(0) == '{' || message.charAt(0) == '[')) // implemented for json objects
                this.message = message;
            else
                this.message = String.format("\"%s\"", message);
        }

        @Override
        public String toString() {
            return String.format("{\"error\": %s}", message);
        }
    }

}

package ru.familyfinance.rest.exception;

/**
 * author: dmitry.yakubovsky
 * date:   04/04/16
 */
public class RequestValidationException extends RuntimeException {

    public RequestValidationException(final String message) {
        super(message);
    }
}

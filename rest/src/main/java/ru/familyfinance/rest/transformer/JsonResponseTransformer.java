package ru.familyfinance.rest.transformer;

import ru.familyfinance.rest.mediatype.MediaType;
import spark.ResponseTransformer;

public class JsonResponseTransformer implements ResponseTransformer {

    public JsonResponseTransformer() {
        super();
    }

    @Override
    public String render(final Object model) {

        return TransformerFactory.getInstance(MediaType.JSON).render(model);
    }

}

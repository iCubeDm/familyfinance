package ru.familyfinance.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.familyfinance.rest.exception.RequestValidationException;
import ru.familyfinance.rest.mediatype.MediaType;
import ru.familyfinance.rest.utils.ErrorUtils;
import spark.ExceptionHandler;
import spark.Request;
import spark.Response;

import static spark.Spark.exception;

public class ExceptionMapper {

    private ExceptionMapper() {
        super();
    }

    static void setup() {

        exception(RuntimeException.class, new DefaultExceptionHandler(500));

        exception(RequestValidationException.class, new DefaultExceptionHandler(400));
    }

    private static final class DefaultExceptionHandler implements ExceptionHandler {

        private final static Logger logger = LoggerFactory.getLogger(DefaultExceptionHandler.class);

        private final int status;

        private final String message;

        private DefaultExceptionHandler(final int status) {
            super();
            this.status = status;
            this.message = null;
        }

        private DefaultExceptionHandler(final int status, final String message) {
            super();
            this.status = status;
            this.message = message;
        }

        @Override
        public void handle(final Exception exception, final Request request, final Response response) {

            response.status(status);
            response.type("application/json");

            logger.error("Exception mapper handled a {}.", exception);
            if (message != null)
                response.body(ErrorUtils.createError(message, MediaType.JSON));
            else
                response.body(ErrorUtils.createError(exception.getMessage(), MediaType.JSON));

        }
    }
}

package ru.familyfinance.rest.resource.category.routes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.familyfinance.domain.category.CategoryService;
import spark.Request;
import spark.Response;
import spark.Route;

/**
 * author: dmitry.yakubovsky
 * date:   19/04/16
 */
public class DeleteCategoryRoute implements Route {

    private final static Logger logger = LoggerFactory.getLogger(DeleteCategoryRoute.class);

    public static final String URL_TEMPLATE = "/api/category/:categoryId";

    private final CategoryService categoryService;

    public DeleteCategoryRoute(CategoryService categoryService) {

        this.categoryService = categoryService;
    }

    @Override
    public Object handle(Request request, Response response) throws Exception {
        final Long categoryId = Long.parseLong(request.params(":categoryId"));

        logger.info("Requested delete category -> {}", categoryId);

        categoryService.deleteCategoryAndClearTransactions(categoryId);

        response.status(200);

        return "{\"status\":\"OK\"}";
    }
}

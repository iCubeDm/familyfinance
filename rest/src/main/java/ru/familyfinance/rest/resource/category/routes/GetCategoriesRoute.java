package ru.familyfinance.rest.resource.category.routes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.familyfinance.domain.category.CategoryService;
import ru.familyfinance.domain.category.contract.CategoryData;
import ru.familyfinance.rest.mediatype.MediaType;
import ru.familyfinance.rest.resource.category.dto.CategoryRest;
import ru.familyfinance.rest.transformer.TransformerFactory;
import spark.Request;
import spark.Response;
import spark.Route;

import java.util.List;
import java.util.stream.Collectors;

/**
 * author: dmitry.yakubovsky
 * date:   19/04/16
 */
public class GetCategoriesRoute implements Route {

    private final static Logger logger = LoggerFactory.getLogger(GetCategoriesRoute.class);

    public static final String URL_TEMPLATE = "/api/categories";

    private final CategoryService categoryService;

    public GetCategoriesRoute(CategoryService categoryService) {

        this.categoryService = categoryService;
    }

    @Override
    public Object handle(Request request, Response response) throws Exception {

        logger.info("Requested categories");

        List<CategoryData> categories = categoryService.findAllCategories();

        List<CategoryRest> responseTxs = categories.stream().map(CategoryRest::new).collect(Collectors.toList());

        String responseBody = TransformerFactory.getInstance(MediaType.JSON).render(responseTxs);
        logger.info("Response categories -> {}", responseBody);

        response.status(200);

        return responseBody;
    }
}

package ru.familyfinance.rest.resource.category.dto;

import ru.familyfinance.domain.category.contract.CategoryData;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * author: dmitry.yakubovsky
 * date:   19/04/16
 */
public class CategoryRest implements CategoryData {

    private Long id;

    @NotNull(message = "Category name must be present")
    @Size(min = 3, message = "Category name must be at least 3 characters")
    private String name;

    public CategoryRest(CategoryData data) {
        id = data.getId();
        name = data.getName();
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }
}

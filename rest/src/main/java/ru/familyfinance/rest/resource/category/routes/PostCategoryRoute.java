package ru.familyfinance.rest.resource.category.routes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.familyfinance.domain.category.CategoryService;
import ru.familyfinance.rest.mediatype.MediaType;
import ru.familyfinance.rest.resource.category.dto.CategoryRest;
import ru.familyfinance.rest.transformer.TransformerFactory;
import ru.familyfinance.rest.utils.RestValidator;
import spark.Request;
import spark.Response;
import spark.Route;

/**
 * author: dmitry.yakubovsky
 * date:   19/04/16
 */
public class PostCategoryRoute implements Route {

    private final static Logger logger = LoggerFactory.getLogger(PostCategoryRoute.class);

    public static final String URL_TEMPLATE = "/api/category";

    private final CategoryService categoryService;

    private final RestValidator restValidator = new RestValidator();

    public PostCategoryRoute(CategoryService categoryService) {

        this.categoryService = categoryService;
    }

    @Override
    public Object handle(Request request, Response response) throws Exception {

        final String requestBody = request.body();

        logger.info("Received category -> {}", requestBody);

        final CategoryRest category = TransformerFactory.getInstance(MediaType.JSON).render(requestBody, CategoryRest.class);

        restValidator.validate(category);

        categoryService.saveCategory(category);

        final String responseBody = "{ \"response\": \"OK\" }";

        logger.info("Response -> {}", responseBody);

        response.status(201);

        return responseBody;
    }
}

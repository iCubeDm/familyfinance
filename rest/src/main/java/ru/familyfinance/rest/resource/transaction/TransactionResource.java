package ru.familyfinance.rest.resource.transaction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.familyfinance.domain.transaction.TransactionService;
import ru.familyfinance.rest.mediatype.MediaType;
import ru.familyfinance.rest.resource.transaction.routes.*;

import static spark.Spark.*;

/**
 * author: dmitry.yakubovsky
 * date:   04/04/16
 */
public class TransactionResource {

    private final static Logger logger = LoggerFactory.getLogger(TransactionResource.class);

    public static void setupEndpoints(TransactionService transactionService) {

        logger.info("Registering Transaction Endpoints...\n");

        get(GetTransactionsMonthReportRoute.URL_TEMPLATE, MediaType.JSON.toString(), new GetTransactionsMonthReportRoute(transactionService));
        logger.info("GET -> '{}' registered\n", GetTransactionsMonthReportRoute.URL_TEMPLATE);

        get(GetTransactionsRoute.URL_TEMPLATE, MediaType.JSON.toString(), new GetTransactionsRoute(transactionService));
        logger.info("GET -> '{}' registered\n", GetTransactionsRoute.URL_TEMPLATE);

        get(GetIdentifyCategoriesRoute.URL_TEMPLATE, MediaType.JSON.toString(), new GetIdentifyCategoriesRoute(transactionService));
        logger.info("GET -> '{}' registered\n", GetIdentifyCategoriesRoute.URL_TEMPLATE);

        post(PostTransactionsRoute.URL_TEMPLATE, MediaType.JSON.toString(), new PostTransactionsRoute(transactionService));
        logger.info("POST -> '{}' registered\n", PostTransactionsRoute.URL_TEMPLATE);

        post(PostClearTransactionCategoryRoute.URL_TEMPLATE, MediaType.JSON.toString(), new PostClearTransactionCategoryRoute(transactionService));
        logger.info("POST -> '{}' registered\n", PostClearTransactionCategoryRoute.URL_TEMPLATE);

        post(PostTransactionRoute.URL_TEMPLATE, MediaType.JSON.toString(), new PostTransactionRoute(transactionService));
        logger.info("POST -> '{}' registered\n", PostTransactionRoute.URL_TEMPLATE);

        post(PostTransactionsReportRoute.URL_TEMPLATE, MediaType.MULTIPART.toString(), new PostTransactionsReportRoute(transactionService));
        logger.info("POST -> '{}' registered\n", PostTransactionsReportRoute.URL_TEMPLATE);

        delete(DeleteTransactionRoute.URL_TEMPLATE, new DeleteTransactionRoute(transactionService));
        logger.info("DELETE -> '{}' registered\n", DeleteTransactionRoute.URL_TEMPLATE);
    }
}

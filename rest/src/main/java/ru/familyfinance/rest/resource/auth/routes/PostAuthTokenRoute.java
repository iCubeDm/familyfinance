package ru.familyfinance.rest.resource.auth.routes;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.familyfinance.domain.utils.DateTime;
import ru.familyfinance.rest.filters.ApiAuthenticationFilter;
import ru.familyfinance.rest.mediatype.MediaType;
import ru.familyfinance.rest.transformer.TransformerFactory;
import spark.Request;
import spark.Response;
import spark.Route;

import java.util.Base64;

import static spark.Spark.halt;

/**
 * author: dmitry.yakubovsky
 * date:   12/04/16
 */
public class PostAuthTokenRoute implements Route {

    private final static Logger logger = LoggerFactory.getLogger(PostAuthTokenRoute.class);

    public static final String URL_TEMPLATE = "/auth/token";

    @Override
    public Object handle(Request request, Response response) throws Exception {

        try {
            String authHeader = request.headers("Authorization");

            final boolean isBlank = StringUtils.isBlank(authHeader);
            final boolean isValidBearerFormat = authHeader.matches("Basic \\S+");

            if (isBlank || !isValidBearerFormat) {
                throw new Exception();
            }

            final String credentialsString = new String(Base64.getDecoder().decode(authHeader.split(" ")[1]));

            if (!credentialsString.matches("\\w+:\\w+")) {
                throw new Exception();
            }

            final String login = credentialsString.split(":")[0];
            final String pass = credentialsString.split(":")[1];

            final String tokenString = ApiAuthenticationFilter.generateUserToken(login, pass);

            if (!ApiAuthenticationFilter.isValidToken(tokenString)) {
                throw new Exception();
            }

            final long expired = DateTime.now().plusHours(2).millis();


            return TransformerFactory.getInstance(MediaType.JSON).render(new TokenInfo(tokenString, expired));

        } catch (Exception ex) {
            logger.warn("Detected unauthorized request from -> {}", request.ip());
            halt(401);
            return null;
        }

    }

    private static class TokenInfo {
        private String token;

        public TokenInfo(String token, long expired) {
            this.token = new String(Base64.getEncoder().encode(String.format("%s:%s", token, expired).getBytes()));
        }
    }
}

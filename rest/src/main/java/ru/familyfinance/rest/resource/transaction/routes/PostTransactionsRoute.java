package ru.familyfinance.rest.resource.transaction.routes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.familyfinance.domain.transaction.TransactionService;
import ru.familyfinance.domain.transaction.contract.TransactionData;
import ru.familyfinance.rest.mediatype.MediaType;
import ru.familyfinance.rest.resource.transaction.dto.TransactionRest;
import ru.familyfinance.rest.transformer.TransformerFactory;
import ru.familyfinance.rest.utils.RestValidator;
import spark.Request;
import spark.Response;
import spark.Route;

import java.util.Arrays;
import java.util.List;

/**
 * author: dmitry.yakubovsky
 * date:   04/04/16
 */
public class PostTransactionsRoute implements Route {

    public static final String URL_TEMPLATE = "/api/transactions";

    private final static Logger logger = LoggerFactory.getLogger(PostTransactionsRoute.class);

    private final TransactionService transactionService;

    private final RestValidator restValidator = new RestValidator();

    public PostTransactionsRoute(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @Override
    public Object handle(Request request, Response response) throws Exception {

        final String requestBody = request.body();

        logger.info("Received -> {}", requestBody);

        final TransactionRest[] txs = TransformerFactory.getInstance(MediaType.JSON).render(requestBody, TransactionRest[].class);

        restValidator.validateBatch(Arrays.asList(txs));

        List<TransactionData> txsToSave = Arrays.asList(txs);

        transactionService.saveTransactionBatch(txsToSave);

        final String responseBody = "{ \"response\": \"OK\" }";
        logger.info("Response -> {}", responseBody);

        response.status(201);

        return responseBody;
    }
}

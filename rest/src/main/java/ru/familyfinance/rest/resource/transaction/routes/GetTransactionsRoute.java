package ru.familyfinance.rest.resource.transaction.routes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.familyfinance.domain.transaction.TransactionService;
import ru.familyfinance.domain.transaction.contract.TransactionData;
import ru.familyfinance.domain.utils.DateTime;
import ru.familyfinance.rest.mediatype.MediaType;
import ru.familyfinance.rest.resource.transaction.dto.TransactionRest;
import ru.familyfinance.rest.transformer.TransformerFactory;
import spark.Request;
import spark.Response;
import spark.Route;

import java.util.List;
import java.util.stream.Collectors;

/**
 * author: dmitry.yakubovsky
 * date:   04/04/16
 */
public class GetTransactionsRoute implements Route {

    public static final String URL_TEMPLATE = "/api/transactions/:fromDate/:toDate";

    private final static Logger logger = LoggerFactory.getLogger(GetTransactionsRoute.class);

    private final TransactionService transactionService;

    public GetTransactionsRoute(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @Override
    public Object handle(Request request, Response response) throws Exception {

        final String fromDateStr = request.params(":fromDate");
        final String toDateStr = request.params(":toDate");

        logger.info("Requested transactions for period -> {} - {}", fromDateStr, toDateStr);

        DateTime fromDate = DateTime.parse(fromDateStr);
        DateTime toDate = DateTime.parse(toDateStr);

        List<TransactionData> txs = transactionService.findTransactionsBetweenDates(fromDate, toDate);

        List<TransactionRest> responseTxs = txs.stream().map(TransactionRest::new).collect(Collectors.toList());

        String responseBody = TransformerFactory.getInstance(MediaType.JSON).render(responseTxs);
        logger.info("Response transactions -> {}", responseBody);

        response.status(200);

        return responseBody;
    }
}

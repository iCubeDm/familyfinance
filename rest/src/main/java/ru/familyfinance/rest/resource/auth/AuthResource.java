package ru.familyfinance.rest.resource.auth;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.familyfinance.rest.mediatype.MediaType;
import ru.familyfinance.rest.resource.auth.routes.PostAuthTokenRoute;

import static spark.Spark.post;

/**
 * author: dmitry.yakubovsky
 * date:   12/04/16
 */
public class AuthResource {

    private final static Logger logger = LoggerFactory.getLogger(AuthResource.class);

    public static void setupEndpoints() {

        logger.info("Registering Auth Endpoints...\n");

        post(PostAuthTokenRoute.URL_TEMPLATE, MediaType.JSON.toString(), new PostAuthTokenRoute());
        logger.info("POST -> '{}' registered\n", PostAuthTokenRoute.URL_TEMPLATE);
    }
}

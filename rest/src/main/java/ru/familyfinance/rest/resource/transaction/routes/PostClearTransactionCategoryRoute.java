package ru.familyfinance.rest.resource.transaction.routes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.familyfinance.domain.transaction.TransactionService;
import ru.familyfinance.rest.mediatype.MediaType;
import ru.familyfinance.rest.resource.transaction.dto.TransactionRest;
import ru.familyfinance.rest.transformer.TransformerFactory;
import spark.Request;
import spark.Response;
import spark.Route;

/**
 * author: dmitry.yakubovsky
 * date:   04/04/16
 */
public class PostClearTransactionCategoryRoute implements Route {

    public static final String URL_TEMPLATE = "/api/transaction/clear-category";

    private final static Logger logger = LoggerFactory.getLogger(PostClearTransactionCategoryRoute.class);

    private final TransactionService transactionService;

    public PostClearTransactionCategoryRoute(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @Override
    public Object handle(Request request, Response response) throws Exception {

        final String requestBody = request.body();

        logger.info("Requested clear category for Transaction -> {}", requestBody);

        TransactionRest transaction = TransformerFactory.getInstance(MediaType.JSON).render(requestBody, TransactionRest.class);

        transactionService.clearTransactionCategory(transaction);

        response.status(200);

        return "{\"response\":\"OK\"}";
    }
}

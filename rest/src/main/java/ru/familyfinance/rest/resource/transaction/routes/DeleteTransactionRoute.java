package ru.familyfinance.rest.resource.transaction.routes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.familyfinance.domain.transaction.TransactionService;
import ru.familyfinance.rest.utils.RestValidator;
import spark.Request;
import spark.Response;
import spark.Route;

/**
 * author: dmitry.yakubovsky
 * date:   11/04/16
 */
public class DeleteTransactionRoute implements Route {

    public static final String URL_TEMPLATE = "/api/transaction/:transactionId";

    private final static Logger logger = LoggerFactory.getLogger(DeleteTransactionRoute.class);

    private final TransactionService transactionService;

    private final RestValidator restValidator = new RestValidator();

    public DeleteTransactionRoute(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @Override
    public Object handle(Request request, Response response) throws Exception {

        final String transactionId = request.params(":transactionId");

        logger.info("Requested delete transaction -> {}", transactionId);

        transactionService.deleteTransaction(transactionId);

        final String responseBody = "{ \"response\": \"OK\" }";
        logger.info("Response -> {}", responseBody);

        response.status(200);

        return responseBody;
    }
}

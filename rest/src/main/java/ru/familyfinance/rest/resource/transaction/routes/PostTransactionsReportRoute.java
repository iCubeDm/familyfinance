package ru.familyfinance.rest.resource.transaction.routes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.familyfinance.domain.api.commerzbank.csv.Bank;
import ru.familyfinance.domain.transaction.TransactionService;
import spark.Request;
import spark.Response;
import spark.Route;

import javax.servlet.MultipartConfigElement;
import javax.servlet.http.Part;
import java.io.InputStreamReader;

/**
 * author: dmitry.yakubovsky
 * date:   11/04/16
 */
public class PostTransactionsReportRoute implements Route {

    public static final String URL_TEMPLATE = "/api/transactions/upload";

    private final static Logger logger = LoggerFactory.getLogger(PostTransactionsReportRoute.class);

    private final TransactionService transactionService;

    public PostTransactionsReportRoute(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @Override
    public Object handle(Request request, Response response) throws Exception {

        logger.info("Requested report upload");
        final String bankName = request.headers("Bank");

        if (request.raw().getAttribute("org.eclipse.jetty.multipartConfig") == null) {
            MultipartConfigElement multipartConfigElement = new MultipartConfigElement(System.getProperty("java.io.tmpdir"));
            request.raw().setAttribute("org.eclipse.jetty.multipartConfig", multipartConfigElement);
        }

        Part part = request.raw().getPart("file");
        final InputStreamReader reader = new InputStreamReader(part.getInputStream());

        Bank bank = Bank.valueOf(bankName);
        transactionService.saveReport(reader, bank);

        return "{\"response\":\"OK\"}";
    }
}

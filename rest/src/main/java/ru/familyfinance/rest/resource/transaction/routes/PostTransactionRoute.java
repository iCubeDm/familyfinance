package ru.familyfinance.rest.resource.transaction.routes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.familyfinance.domain.transaction.TransactionService;
import ru.familyfinance.rest.mediatype.MediaType;
import ru.familyfinance.rest.resource.transaction.dto.TransactionRest;
import ru.familyfinance.rest.transformer.TransformerFactory;
import ru.familyfinance.rest.utils.RestValidator;
import spark.Request;
import spark.Response;
import spark.Route;

/**
 * author: dmitry.yakubovsky
 * date:   04/04/16
 */
public class PostTransactionRoute implements Route {

    public static final String URL_TEMPLATE = "/api/transaction";

    private final static Logger logger = LoggerFactory.getLogger(PostTransactionRoute.class);

    private final TransactionService transactionService;

    private final RestValidator restValidator = new RestValidator();

    public PostTransactionRoute(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @Override
    public Object handle(Request request, Response response) throws Exception {

        final String requestBody = request.body();

        logger.info("Received -> {}", requestBody);

        final TransactionRest tx = TransformerFactory.getInstance(MediaType.JSON).render(requestBody, TransactionRest.class);

        restValidator.validate(tx);

        transactionService.saveTransactionManual(tx);

        final String responseBody = "{ \"response\": \"OK\" }";
        logger.info("Response -> {}", responseBody);

        response.status(201);

        return responseBody;
    }
}

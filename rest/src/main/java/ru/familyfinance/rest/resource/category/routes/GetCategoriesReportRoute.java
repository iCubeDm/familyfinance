package ru.familyfinance.rest.resource.category.routes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.familyfinance.domain.category.CategoryService;
import ru.familyfinance.domain.category.contract.CategoryReport;
import ru.familyfinance.domain.utils.DateTime;
import ru.familyfinance.rest.mediatype.MediaType;
import ru.familyfinance.rest.transformer.TransformerFactory;
import spark.Request;
import spark.Response;
import spark.Route;

/**
 * author: dmitry.yakubovsky
 * date:   19/04/16
 */
public class GetCategoriesReportRoute implements Route {

    private final static Logger logger = LoggerFactory.getLogger(GetCategoriesReportRoute.class);

    public static final String URL_TEMPLATE = "/api/categories/report/:fromDate/:toDate";

    private final CategoryService categoryService;

    public GetCategoriesReportRoute(CategoryService categoryService) {

        this.categoryService = categoryService;
    }

    @Override
    public Object handle(Request request, Response response) throws Exception {

        String fromDateString = request.params(":fromDate");

        String toDateString = request.params(":toDate");

        logger.info("Requested categories report for period -> {}/{}", fromDateString, toDateString);


        DateTime fromDate = DateTime.parse(fromDateString);
        DateTime toDate = DateTime.parse(toDateString);

        CategoryReport report = categoryService.getReportForPeriod(fromDate, toDate);

        String responseBody = TransformerFactory.getInstance(MediaType.JSON).render(report);
        logger.info("Response categories -> {}", responseBody);

        response.status(200);

        return responseBody;
    }
}

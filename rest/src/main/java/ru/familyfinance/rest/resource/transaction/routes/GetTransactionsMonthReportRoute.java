package ru.familyfinance.rest.resource.transaction.routes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.familyfinance.domain.transaction.contract.MonthReport;
import ru.familyfinance.domain.transaction.TransactionService;
import ru.familyfinance.domain.utils.DateTime;
import ru.familyfinance.rest.mediatype.MediaType;
import ru.familyfinance.rest.transformer.TransformerFactory;
import spark.Request;
import spark.Response;
import spark.Route;

/**
 * author: dmitry.yakubovsky
 * date:   19/04/16
 */
public class GetTransactionsMonthReportRoute implements Route {

    private final static Logger logger = LoggerFactory.getLogger(GetTransactionsMonthReportRoute.class);

    public static final String URL_TEMPLATE = "/api/transactions/reports/months/:fromDate/:toDate";

    private final TransactionService transactionService;

    public GetTransactionsMonthReportRoute(TransactionService transactionService) {

        this.transactionService = transactionService;
    }

    @Override
    public Object handle(Request request, Response response) throws Exception {

        String fromDateString = request.params(":fromDate");

        String toDateString = request.params(":toDate");

        logger.info("Requested categories report for period -> {}/{}", fromDateString, toDateString);


        DateTime fromDate = DateTime.parse(fromDateString);
        DateTime toDate = DateTime.parse(toDateString);

        MonthReport report = transactionService.getMonthReportForPeriod(fromDate, toDate);

        String responseBody = TransformerFactory.getInstance(MediaType.JSON).render(report);
        logger.info("Response categories -> {}", responseBody);

        response.status(200);

        return responseBody;
    }
}

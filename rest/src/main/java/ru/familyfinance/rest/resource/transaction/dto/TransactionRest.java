package ru.familyfinance.rest.resource.transaction.dto;

import org.hibernate.validator.constraints.NotBlank;
import ru.familyfinance.domain.transaction.contract.TransactionData;
import ru.familyfinance.domain.utils.DateTime;
import ru.familyfinance.domain.utils.Money;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * author: dmitry.yakubovsky
 * date:   10/04/16
 * 2015-08-11
 */
public class TransactionRest implements TransactionData {

    private String id;

    @NotBlank
    private String amount;

    @NotNull
    private String description;

    @Pattern(regexp = "\\d{4}-\\d{2}-\\d{2}(T.+Z)?")
    private String executionDate;

    private Long categoryId;

    private Boolean categoryApproved = false;

    @NotNull
    private Boolean active = true;

    public TransactionRest() {
    }

    public TransactionRest(TransactionData data) {

        this.id = data.getId();
        this.amount = data.getAmount().getAmount();
        this.executionDate = data.getExecutionDate().toString();
        this.categoryId = data.getCategoryId();
        this.description = data.getDescription();
        this.active = data.getActive();
        this.categoryApproved = data.getCategoryApproved();
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public Money getAmount() {
        return Money.valueOf(amount);
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public DateTime getExecutionDate() {
        return DateTime.parse(executionDate);
    }

    @Override
    public Long getCategoryId() {
        return categoryId;
    }

    @Override
    public Boolean getActive() {
        return active;
    }

    @Override
    public Boolean getCategoryApproved() {
        return categoryApproved;
    }
}

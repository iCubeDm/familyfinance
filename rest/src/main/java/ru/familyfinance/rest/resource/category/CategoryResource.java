package ru.familyfinance.rest.resource.category;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.familyfinance.domain.category.CategoryService;
import ru.familyfinance.rest.mediatype.MediaType;
import ru.familyfinance.rest.resource.category.routes.DeleteCategoryRoute;
import ru.familyfinance.rest.resource.category.routes.GetCategoriesReportRoute;
import ru.familyfinance.rest.resource.category.routes.GetCategoriesRoute;
import ru.familyfinance.rest.resource.category.routes.PostCategoryRoute;

import static spark.Spark.*;

/**
 * author: dmitry.yakubovsky
 * date:   19/04/16
 */
public class CategoryResource {

    private final static Logger logger = LoggerFactory.getLogger(CategoryResource.class);

    public static void setupEndpoints(CategoryService categoryService) {

        logger.info("Registering Category Endpoints...\n");

        get(GetCategoriesRoute.URL_TEMPLATE, MediaType.JSON.toString(), new GetCategoriesRoute(categoryService));
        logger.info("GET -> '{}' registered\n", GetCategoriesRoute.URL_TEMPLATE);

        get(GetCategoriesReportRoute.URL_TEMPLATE, MediaType.JSON.toString(), new GetCategoriesReportRoute(categoryService));
        logger.info("GET -> '{}' registered\n", GetCategoriesReportRoute.URL_TEMPLATE);

        post(PostCategoryRoute.URL_TEMPLATE, MediaType.JSON.toString(), new PostCategoryRoute(categoryService));
        logger.info("POST -> '{}' registered\n", PostCategoryRoute.URL_TEMPLATE);

        delete(DeleteCategoryRoute.URL_TEMPLATE, MediaType.JSON.toString(), new DeleteCategoryRoute(categoryService));
        logger.info("DELETE -> '{}' registered\n", DeleteCategoryRoute.URL_TEMPLATE);
    }
}


package ru.familyfinance.rest.resource.transaction.routes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.familyfinance.domain.transaction.TransactionService;
import spark.Request;
import spark.Response;
import spark.Route;

/**
 * author: dmitry.yakubovsky
 * date:   19/04/16
 */
public class GetIdentifyCategoriesRoute implements Route {

    private final static Logger logger = LoggerFactory.getLogger(GetIdentifyCategoriesRoute.class);

    public static final String URL_TEMPLATE = "/api/transactions/identify-categories";

    private final TransactionService transactionService;

    public GetIdentifyCategoriesRoute(TransactionService transactionService) {

        this.transactionService = transactionService;
    }

    @Override
    public Object handle(Request request, Response response) throws Exception {

        logger.info("Requested category identification");

        transactionService.identifyCategories();

        response.status(200);

        return "{\"status\":\"OK\"}";
    }
}

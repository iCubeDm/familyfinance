package ru.familyfinance.domain.repository;

import ru.familyfinance.domain.category.contract.CategoryData;

import java.util.List;

/**
 * author: dmitry.yakubovsky
 * date:   11/04/16
 */
public interface CategoryRepository {
    List<CategoryData> findAll();

    void saveCategory(CategoryData category);

    void deleteCategory(CategoryData category);

    CategoryData findById(Long categoryId);
}

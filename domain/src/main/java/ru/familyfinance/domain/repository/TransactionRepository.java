package ru.familyfinance.domain.repository;

import ru.familyfinance.domain.category.contract.CategoryData;
import ru.familyfinance.domain.transaction.contract.TransactionData;
import ru.familyfinance.domain.utils.DateTime;

import java.util.List;

/**
 * author: dmitry.yakubovsky
 * date:   09/04/16
 */
public interface TransactionRepository {

    void saveUnique(List<TransactionData> transactions);

    void saveOrUpdate(List<TransactionData> transactions);

    void saveOrUpdate(TransactionData transaction);

    List<TransactionData> fetchAllByInterval(DateTime from, DateTime to);

    List<TransactionData> findAllCategorizedTransactions();

    List<TransactionData> findAllUncategorizedTransactions();

    void createTransaction(TransactionData transactionData);

    void deactivateTransaction(String transactionId);

    List<TransactionData> findAllTransactionsWithCategory(CategoryData category);

}

package ru.familyfinance.domain.category.contract;

import ru.familyfinance.domain.utils.Money;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * author: dmitry.yakubovsky
 * date:   23/04/16
 */
public class CategoryReport {

    private List<ReportRow> revenues = new ArrayList<>();
    private List<ReportRow> expenses = new ArrayList<>();

    public CategoryReport() {
    }

    public CategoryReport addRevenue(String categoryName, Money summaryAmount) {
        if (summaryAmount.compareTo(Money.zero()) < 0) {
            throw new IllegalArgumentException("revenue can not be less than 0");
        }
        revenues.add(new ReportRow(categoryName, summaryAmount.getAmount()));
        revenues = revenues.stream()
                .sorted((row1, row2) ->
                        new BigDecimal(row2.getSumAmount()).compareTo(new BigDecimal(row1.getSumAmount()))
                )
                .collect(Collectors.toList());
        return this;
    }

    public CategoryReport addExpense(String categoryName, Money summaryAmount) {
        if (summaryAmount.compareTo(Money.zero()) > 0) {
            throw new IllegalArgumentException("expense can not be greater or equals 0");
        }
        expenses.add(new ReportRow(categoryName, summaryAmount.getAmount()));
        expenses = expenses.stream()
                .sorted((row1, row2) ->
                        new BigDecimal(row1.getSumAmount()).compareTo(new BigDecimal(row2.getSumAmount()))
                )
                .collect(Collectors.toList());
        return this;
    }

    public List<ReportRow> getRevenues() {
        return revenues;
    }

    public List<ReportRow> getExpenses() {
        return expenses;
    }

    public static class ReportRow {
        private String categoryName;
        private String sumAmount;

        public ReportRow(String categoryName, String sumAmount) {
            this.categoryName = categoryName;
            this.sumAmount = sumAmount;
        }

        public String getCategoryName() {
            return categoryName;
        }

        public String getSumAmount() {
            return sumAmount;
        }
    }
}

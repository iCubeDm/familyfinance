package ru.familyfinance.domain.category;

import ru.familyfinance.domain.DomainService;
import ru.familyfinance.domain.category.contract.CategoryData;
import ru.familyfinance.domain.category.contract.CategoryReport;
import ru.familyfinance.domain.repository.CategoryRepository;
import ru.familyfinance.domain.repository.TransactionRepository;
import ru.familyfinance.domain.transaction.TransactionDomain;
import ru.familyfinance.domain.transaction.contract.TransactionData;
import ru.familyfinance.domain.utils.DateTime;
import ru.familyfinance.domain.utils.Money;

import java.util.List;
import java.util.stream.Collectors;

/**
 * author: dmitry.yakubovsky
 * date:   11/04/16
 */
public class CategoryService implements DomainService {

    private CategoryRepository categoryRepository;
    private TransactionRepository transactionRepository;

    public CategoryService(CategoryRepository categoryRepository, TransactionRepository transactionRepository) {
        this.categoryRepository = categoryRepository;
        this.transactionRepository = transactionRepository;
    }


    public List<CategoryData> findAllCategories() {

        return categoryRepository.findAll();
    }

    public void saveCategory(CategoryData data) {
        categoryRepository.saveCategory(data);
    }

    public void deleteCategoryAndClearTransactions(Long categoryId) {

        CategoryData category = categoryRepository.findById(categoryId);
        if (category == null) {
            return;
        }

        List<TransactionData> transactions = transactionRepository.findAllTransactionsWithCategory(category);

        List<TransactionDomain> txDomain = transactions.stream().map(TransactionDomain::new).collect(Collectors.toList());

        txDomain.forEach(TransactionDomain::clearCategory);

        transactionRepository.saveOrUpdate(txDomain.stream().map(TransactionDomain::toInterface).collect(Collectors.toList()));

        categoryRepository.deleteCategory(category);
    }

    public CategoryReport getReportForPeriod(DateTime fromDate, DateTime toDate) {

        CategoryReport report = new CategoryReport();

        List<CategoryData> categories = this.findAllCategories();
        List<TransactionData> transactionsForPeriod = transactionRepository.fetchAllByInterval(fromDate, toDate);

        Money sumExpensesUndefined = Money.zero();
        Money sumRevenuesUndefined = Money.zero();
        transactionsForPeriod.stream()
                .filter((tx) -> tx.getCategoryId() == 0L)
                .forEach((tx) -> {
                    if (tx.getAmount().compareTo(Money.zero()) < 0) {
                        sumExpensesUndefined.add(tx.getAmount());
                    } else {
                        sumRevenuesUndefined.add(tx.getAmount());
                    }
                });

        report.addRevenue("Undefined", sumRevenuesUndefined);
        report.addExpense("Undefined", sumExpensesUndefined);

        for (CategoryData category : categories) {
            Money sumRevenues = Money.zero();
            Money sumExpenses = Money.zero();

            transactionsForPeriod.stream()
                    .filter((tx) -> tx.getCategoryId().equals(category.getId()))
                    .forEach((tx) -> {
                        if (tx.getAmount().compareTo(Money.zero()) < 0) {
                            sumExpenses.add(tx.getAmount());
                        } else {
                            sumRevenues.add(tx.getAmount());
                        }
                    });

            report.addRevenue(category.getName(), sumRevenues);
            report.addExpense(category.getName(), sumExpenses);
        }

        return report;
    }
}

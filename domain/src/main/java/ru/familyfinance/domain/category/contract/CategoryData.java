package ru.familyfinance.domain.category.contract;

/**
 * author: dmitry.yakubovsky
 * date:   11/04/16
 */
public interface CategoryData {

    Long getId();

    String getName();
}

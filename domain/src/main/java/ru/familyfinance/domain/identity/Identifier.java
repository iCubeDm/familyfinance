package ru.familyfinance.domain.identity;

import java.util.List;
import java.util.Objects;

import static java.util.UUID.randomUUID;
import static ru.familyfinance.domain.identity.Provider.SHA1;

public class Identifier {

    private final String content;

    public Identifier(final String content) {

        this.content = content;
    }

    public static Identifier of(final String... source) {

        return new Identifier(SHA1.encode(source));
    }

    public static Identifier of(final List<String> source) {

        return new Identifier(SHA1.encode(source.toArray(new String[source.size()])));
    }

    public static Identifier random() {

        return new Identifier(SHA1.encode(randomUUID().toString()));
    }

    public String get() {

        return content;
    }

    @Override
    public boolean equals(final Object obj) {

        if (this == obj) return true;
        if (!(obj instanceof Identifier)) return false;

        return Objects.equals(content, ((Identifier) obj).content);
    }

    @Override
    public int hashCode() {
        return content.hashCode();
    }

    @Override
    public String toString() {

        return content;
    }
}

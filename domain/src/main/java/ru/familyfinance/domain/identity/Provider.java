package ru.familyfinance.domain.identity;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

enum Provider {

    SHA1;

    private final MessageDigest sha1;

    Provider() {

        try {

            this.sha1 = MessageDigest.getInstance("SHA1");

        } catch (final NoSuchAlgorithmException ex) {

            throw new IllegalStateException(ex);
        }
    }

    public String encode(final String... data) {

        final StringBuilder sb = new StringBuilder(data[0]);

        if (data.length > 1) {
            for (int i = 1; i < data.length; i++)
                sb.append("-").append(data[i]);
        }

        return encode(sb.toString());
    }

    public synchronized String encode(final String data) {

        return byteArrayToHexString(sha1.digest(data.getBytes()));
    }

    private static String byteArrayToHexString(final byte[] bytes) {

        final StringBuilder result = new StringBuilder();

        for (final byte aByte : bytes)
            result.append(Integer.toString((aByte & 0xff) + 0x100, 16).substring(1));

        return result.toString();
    }
}

package ru.familyfinance.domain.utils;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Objects;

public class DateTime {

    private static final ZoneId DEFAULT_TIME_ZONE = ZoneOffset.UTC;

    private final ZonedDateTime zonedDateTime;

    private DateTime(final ZonedDateTime zonedDateTime) {

        this.zonedDateTime = zonedDateTime;
    }

    public static DateTime ofEpochMillis(long epochMilli) {

        return new DateTime(ZonedDateTime.ofInstant(Instant.ofEpochMilli(epochMilli), DEFAULT_TIME_ZONE));
    }

    /**
     * The string must represent a valid date-time and is parsed using one of the following:
     * <ul>
     * <li>{@link java.time.LocalDate#parse(CharSequence)}.</li>
     * <li>{@link java.time.LocalDateTime#parse(CharSequence)}.</li>
     * <li>{@link java.time.ZonedDateTime#parse(CharSequence)}.</li>
     * </ul>
     * The class to be parsed depends on how much information is passed to the iso8601DateTime argument.
     * <p>
     * Default time zone is ZoneOffset.UTC.
     *
     * @param iso8601DateTime the text to parse such as "2007-12-03T10:15:30+01:00[Europe/Paris]", not null
     * @return the parsed zoned date-time, not null
     * @throws DateTimeParseException if the text cannot be parsed
     */
    public static DateTime parse(final String iso8601DateTime) {

        final ZonedDateTime zonedDateTime;

        if (iso8601DateTime.length() <= 11) {

            zonedDateTime = ZonedDateTime.of(LocalDate.parse(iso8601DateTime), LocalTime.of(0, 0), DEFAULT_TIME_ZONE);

        } else if (iso8601DateTime.contains("Z") || iso8601DateTime.contains("+")) {

            zonedDateTime = ZonedDateTime.parse(iso8601DateTime);

        } else {

            zonedDateTime = ZonedDateTime.of(LocalDateTime.parse(iso8601DateTime), DEFAULT_TIME_ZONE);
        }

        return new DateTime(zonedDateTime);
    }

    public static DateTime parse(final String date, final String format) {

        final ZonedDateTime zonedDateTime;

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);

        zonedDateTime = ZonedDateTime.of(LocalDate.parse(date, formatter).atStartOfDay(), DEFAULT_TIME_ZONE);

        return new DateTime(zonedDateTime);
    }

    public static DateTime now() {

        return new DateTime(ZonedDateTime.now(DEFAULT_TIME_ZONE));
    }

    public String format(String format) {
        return zonedDateTime.format(DateTimeFormatter.ofPattern(format));
    }

    public String rfc1123() {

        return zonedDateTime.format(DateTimeFormatter.RFC_1123_DATE_TIME);
    }

    public String iso8601() {

        return zonedDateTime.toString();
    }

    public long millis() {
        return this.zonedDateTime.toInstant().toEpochMilli();
    }

    public Boolean isBefore(DateTime date) {
        return this.zonedDateTime.isBefore(date.zonedDateTime);
    }

    public Boolean isAfter(DateTime date) {
        return this.zonedDateTime.isAfter(date.zonedDateTime);
    }

    @Override
    public String toString() {

        return zonedDateTime.toString();
    }

    @Override
    public boolean equals(final Object obj) {

        return this == obj || obj instanceof DateTime && zonedDateTime.equals(((DateTime) obj).zonedDateTime);
    }

    @Override
    public int hashCode() {

        return Objects.hash(zonedDateTime);
    }

    public DateTime plusHours(int hours) {

        return new DateTime(zonedDateTime.plusHours(hours));
    }
}

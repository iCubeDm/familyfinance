package ru.familyfinance.domain.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * author: dmitry.yakubovsky
 * date:   14/04/16
 */
public class Money {
    private static int FRACTIONS_SCALE = 11;
    private static int AMOUNT_SCALE = 2;

    private BigDecimal amount;

    private Money(final BigDecimal amount) {
        super();
        this.amount = amount.setScale(FRACTIONS_SCALE, RoundingMode.HALF_UP);
    }

    public Money(final String amount) {
        super();
        this.amount = new BigDecimal(amount).setScale(FRACTIONS_SCALE, RoundingMode.HALF_UP);
    }

    public static Money valueOf(final String amount) {
        return new Money(amount);
    }

    public static Money valueOf(final BigDecimal amount) {
        return new Money(amount);
    }

    public Money add(final Money money) {
        this.amount = this.amount.add(money.amount);
        return this;
    }

    public Money subtract(final Money money) {
        this.amount = this.amount.subtract(money.amount);
        return this;
    }

    public int compareTo(Money money) {
        return this.amount.compareTo(money.amount);
    }

    public String getAmount() {
        BigDecimal scaledAmount = amount;
        return scaledAmount.setScale(AMOUNT_SCALE, BigDecimal.ROUND_HALF_UP).toPlainString();
    }

    public boolean greaterThan(final Money money) {

        return this.amount.compareTo(money.amount) > 0;
    }

    public boolean lessThan(final Money money) {

        return this.amount.compareTo(money.amount) < 0;
    }

    public String getAmountWithFractions() {
        return amount.toPlainString();
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Money other = (Money) obj;
        if (amount == null) {
            if (other.amount != null)
                return false;
        } else if (amount.compareTo(other.amount) != 0)
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((amount == null) ? 0 : amount.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return amount.toPlainString();
    }

    public static Money zero() {
        return new Money("0");
    }
}

package ru.familyfinance.domain.utils;

import ru.familyfinance.domain.transaction.TransactionDomain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.zip.CRC32;

/**
 * author: dmitry.yakubovsky
 * date:   11/04/16
 */
public class CategoryIdentificationUtils {

    public static float SIMILARITY_VALUE_THRESHOLD = 70f;

    public static float getSimilarityIndex(TransactionDomain firstEntry, TransactionDomain secondEntry) {

        List<Long> hashedWords1 = firstEntry.getDescriptionHashes();

        List<Long> hashedWords2 = secondEntry.getDescriptionHashes();

        Long intersections = hashedWords1.stream().filter(hashedWords2::contains).count();

        float length = Math.min(hashedWords1.size(), hashedWords2.size()) * 1.0f;

        return intersections / length * 100;
    }

    public static float getSimilarityIndex(String first, String second) {

        List<Long> hashedWords1 = hashDescription(first);

        List<Long> hashedWords2 = hashDescription(second);

        Long intersections = hashedWords1.stream().filter(hashedWords2::contains).count();

        float length = Math.min(hashedWords1.size(), hashedWords2.size()) * 1.0f;

        return intersections / length * 100;
    }

    public static List<Long> hashDescription(String rawDescription) {

        final List<String> clearedStrings = Arrays.asList(rawDescription.replaceAll(",|-|_|:|Kartenzahlung", " ")
                .replaceAll("\\.|/|\\||\\?|!|\\(|\\)|\\d|Kartenzahlung", "").toUpperCase()
                .split(" "));

        final List<String> canonizedWords = clearedStrings.stream()
                .filter(item -> item.length() > 3 && !item.matches("\\d+")
                        || item.equals("GA")
                        || item.equals("GR")
                        || item.equals("HT"))
                .collect(Collectors.toList());

        List<String> hashConsequence = new ArrayList<>();

        for (int i = 0; i < canonizedWords.size(); i++) {
            if (i > 15) break;
            hashConsequence.add(canonizedWords.get(i));
        }

        return hashConsequence.stream().map(word -> {
            CRC32 crc = new CRC32();
            crc.update(word.getBytes());
            return crc.getValue();
        }).collect(Collectors.toList());
    }

}

package ru.familyfinance.domain.transaction.contract;

import ru.familyfinance.domain.utils.DateTime;
import ru.familyfinance.domain.utils.Money;

/**
 * author: dmitry.yakubovsky
 * date:   09/04/16
 */
public interface TransactionData {

    String getId();

    Money getAmount();

    String getDescription();

    DateTime getExecutionDate();

    Long getCategoryId();

    Boolean getActive();

    Boolean getCategoryApproved();


}

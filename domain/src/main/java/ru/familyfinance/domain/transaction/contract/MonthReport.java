package ru.familyfinance.domain.transaction.contract;

import ru.familyfinance.domain.utils.Money;

import java.util.*;

/**
 * author: dmitry.yakubovsky
 * date:   16/07/16
 * TODO ЯРЫЙ СУКА ГОВНОКОД!!!!!!!!!!!
 */
public class MonthReport {

    private Set<Report> expenses = new HashSet<>();
    private Set<Report> revenues = new HashSet<>();

    public void addReport(String month, String category, Money amount) {
        if (amount.lessThan(Money.zero())) {
            String amountStr = amount.getAmount().replace("-", "");
            amount = Money.valueOf(amountStr);
            addToExpenses(new MonthReportRow(category, amount), month);
        } else {
            addToRevenues(new MonthReportRow(category, amount), month);
        }
    }

    private void addToExpenses(MonthReportRow report, String month) {

        final Optional<Report> foundReport = expenses.stream().filter(r -> r.month.equals(month)).findFirst();

        if (foundReport.isPresent()) {
            foundReport.get().addReportRow(report);
            expenses.remove(foundReport.get());
            expenses.add(foundReport.get());
        } else {
            Report newReport = new Report(month);
            newReport.addReportRow(report);
            expenses.remove(newReport);
            expenses.add(newReport);
        }
    }

    private void addToRevenues(MonthReportRow report, String month) {

        final Optional<Report> foundReport = revenues.stream().filter(r -> r.month.equals(month)).findFirst();

        if (foundReport.isPresent()) {
            foundReport.get().addReportRow(report);
            revenues.add(foundReport.get());
        } else {
            Report newReport = new Report(month);
            newReport.addReportRow(report);
            revenues.add(newReport);
        }
    }

    private class Report {
        private final String month;
        private String totalAmount;
        private List<MonthReportRow> categories = new ArrayList<>();

        private Report(String month) {
            this.month = month;
            this.totalAmount = "0";
        }

        void addReportRow(MonthReportRow report) {
            totalAmount = Money.valueOf(totalAmount).add(Money.valueOf(report.amount)).getAmount();
            Optional<MonthReportRow> foundReport = categories.stream().filter(r -> r.category.equals(report.category)).findFirst();
            if (foundReport.isPresent()) {
                foundReport.get().amount = Money.valueOf(foundReport.get().amount).add(Money.valueOf(report.amount)).getAmount();
                categories.remove(foundReport.get());
                categories.add(foundReport.get());
            } else {
                categories.add(report);
            }

        }
    }

    class MonthReportRow {

        private final String category;
        private String amount;

        public MonthReportRow(String category, Money amount) {
            this.category = category;
            this.amount = amount.getAmount();
        }
    }
}

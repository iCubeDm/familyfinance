package ru.familyfinance.domain.transaction;

import ru.familyfinance.domain.transaction.contract.TransactionBehaviour;
import ru.familyfinance.domain.transaction.contract.TransactionData;
import ru.familyfinance.domain.utils.CategoryIdentificationUtils;
import ru.familyfinance.domain.utils.DateTime;
import ru.familyfinance.domain.utils.Money;

import java.util.ArrayList;
import java.util.List;

/**
 * author: dmitry.yakubovsky
 * date:   09/04/16
 */
public class TransactionDomain implements TransactionData, TransactionBehaviour {

    private String id;
    private Money amount;
    private String description;
    private DateTime executionDate;
    private Long categoryId;
    private Boolean active = true;
    private Boolean categoryApproved = false;

    private List<Long> descriptionHashes = new ArrayList<>();

    public TransactionDomain(TransactionData data) {

        this.id = data.getId();
        this.amount = data.getAmount();
        this.executionDate = data.getExecutionDate();
        this.categoryId = data.getCategoryId();
        this.description = data.getDescription();
        this.active = data.getActive();
        this.categoryApproved = data.getCategoryApproved();

        descriptionHashes = CategoryIdentificationUtils.hashDescription(description);
    }

    @Override
    public void identifyCategory(List<TransactionDomain> categorizedTransactions) {

        float max = 0.0f;
        TransactionDomain maxEntry = null;

        for (TransactionDomain possibleSimilarEntry : categorizedTransactions) {
            float similarityIndex = CategoryIdentificationUtils.getSimilarityIndex(this, possibleSimilarEntry);

            if (similarityIndex < CategoryIdentificationUtils.SIMILARITY_VALUE_THRESHOLD
                    || similarityIndex < max
                    || this.getId().equals(possibleSimilarEntry.getId())) {
                continue;
            }

            max = similarityIndex;
            maxEntry = possibleSimilarEntry;
        }

        if (maxEntry != null) {
            this.categoryId = maxEntry.getCategoryId();
        }

        this.categoryApproved = false;
    }

    @Override
    public void approveCategory() {
        categoryApproved = true;
    }

    @Override
    public TransactionData toInterface() {
        return this;
    }

    @Override
    public void clearCategory() {
        categoryId = 0L;
        categoryApproved = false;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public Money getAmount() {
        return amount;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public DateTime getExecutionDate() {
        return executionDate;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    @Override
    public Boolean getActive() {
        return active;
    }

    @Override
    public Boolean getCategoryApproved() {
        return categoryApproved;
    }

    public List<Long> getDescriptionHashes() {
        return descriptionHashes;
    }

    @Override
    public String toString() {
        return "TransactionDomain{" +
                "id='" + id + '\'' +
                ", amount=" + amount +
                ", description='" + description + '\'' +
                ", executionDate=" + executionDate +
                ", categoryId=" + categoryId +
                ", active=" + active +
                ", categoryApproved=" + categoryApproved +
                ", descriptionHashes=" + descriptionHashes +
                '}';
    }
}

package ru.familyfinance.domain.transaction;

import ru.familyfinance.domain.DomainService;
import ru.familyfinance.domain.api.commerzbank.csv.Bank;
import ru.familyfinance.domain.api.commerzbank.csv.ParserFactory;
import ru.familyfinance.domain.category.contract.CategoryData;
import ru.familyfinance.domain.repository.CategoryRepository;
import ru.familyfinance.domain.repository.TransactionRepository;
import ru.familyfinance.domain.transaction.contract.MonthReport;
import ru.familyfinance.domain.transaction.contract.TransactionData;
import ru.familyfinance.domain.utils.DateTime;

import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


public class TransactionService implements DomainService {

    private final TransactionRepository transactionRepository;

    private final CategoryRepository categoryRepository;

    private final ParserFactory parserFactory;

    public TransactionService(final TransactionRepository transactionRepository,
                              final ParserFactory parserFactory,
                              final CategoryRepository categoryRepository) {
        this.transactionRepository = transactionRepository;
        this.parserFactory = parserFactory;
        this.categoryRepository = categoryRepository;
    }

    public void saveTransactionManual(TransactionData transaction) {

        TransactionDomain transactionDomain = new TransactionDomain(transaction);

        if (transactionDomain.getCategoryId() != null && transactionDomain.getCategoryId() != 0L) {
            transactionDomain.approveCategory();
        }

        transactionRepository.createTransaction(transactionDomain);
    }

    public void deleteTransaction(String transactionId) {

        transactionRepository.deactivateTransaction(transactionId);
    }

    public void saveTransactionBatch(List<TransactionData> transactions) {
        transactionRepository.saveOrUpdate(transactions);
    }

    public List<TransactionData> findTransactionsBetweenDates(DateTime fromDate, DateTime toDate) {
        return transactionRepository.fetchAllByInterval(fromDate, toDate);
    }

    public void identifyCategories() {

        List<TransactionDomain> categorizedTransactions = transactionRepository.findAllCategorizedTransactions().stream()
                .map(TransactionDomain::new)
                .collect(Collectors.toList());

        if (categorizedTransactions.isEmpty()) return;

        List<TransactionDomain> uncategorizedTransactions = transactionRepository.findAllUncategorizedTransactions().stream()
                .map(TransactionDomain::new)
                .collect(Collectors.toList());

        uncategorizedTransactions.forEach(tx -> tx.identifyCategory(categorizedTransactions));

        List<TransactionData> transactionsForSave = uncategorizedTransactions.stream()
                .map(TransactionDomain::toInterface)
                .collect(Collectors.toList());

        transactionRepository.saveOrUpdate(transactionsForSave);
    }

    public void saveReport(InputStreamReader rawReport, Bank bank) {

        List<TransactionData> parsedTransactions = parserFactory.of(bank).parseCsvInputStream(rawReport);

        transactionRepository.saveUnique(parsedTransactions);
    }

    public void clearTransactionCategory(TransactionData transaction) {

        TransactionDomain txDomain = new TransactionDomain(transaction);

        txDomain.clearCategory();

        transactionRepository.saveOrUpdate(txDomain);
    }

    public MonthReport getMonthReportForPeriod(DateTime fromDate, DateTime toDate) {

        final String FORMAT = "MM.yyyy";

        MonthReport monthReport = new MonthReport();

        List<TransactionData> foundTransactions = transactionRepository.fetchAllByInterval(fromDate, toDate);

        Set<String> monthsBetween = new HashSet<>();

        foundTransactions.stream().forEach(tx -> monthsBetween.add(tx.getExecutionDate().format(FORMAT)));

        for (String month : monthsBetween) {
            foundTransactions.stream()
                    .filter(tx -> tx.getExecutionDate().format(FORMAT).equals(month))
                    .forEach(tx -> monthReport.addReport(month, categoryNameById(tx.getCategoryId()), tx.getAmount()));
        }


        return monthReport;
    }

    private String categoryNameById(long categoryId) {
        CategoryData category = categoryRepository.findById(categoryId);
        if (category == null) {
            return "Undefined";
        }
        return category.getName();
    }
}

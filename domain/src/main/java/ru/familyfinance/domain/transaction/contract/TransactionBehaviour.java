package ru.familyfinance.domain.transaction.contract;

import ru.familyfinance.domain.transaction.TransactionDomain;

import java.util.List;

/**
 * author: dmitry.yakubovsky
 * date:   09/04/16
 */
public interface TransactionBehaviour {

    void identifyCategory(List<TransactionDomain> categorizedTransactions);

    void approveCategory();

    TransactionData toInterface();

    void clearCategory();

}

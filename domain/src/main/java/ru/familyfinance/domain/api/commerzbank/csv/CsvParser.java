package ru.familyfinance.domain.api.commerzbank.csv;


import ru.familyfinance.domain.transaction.contract.TransactionData;

import java.io.InputStreamReader;
import java.util.List;

/**
 * author: dmitry.yakubovsky
 * date:   16/11/15
 */
public interface CsvParser
{
    List<TransactionData> parseCsvInputStream(InputStreamReader streamReader);
}

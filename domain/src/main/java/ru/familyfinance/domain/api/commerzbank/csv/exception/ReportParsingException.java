package ru.familyfinance.domain.api.commerzbank.csv.exception;

/**
 * author: dmitry.yakubovsky
 * date:   11/04/16
 */
public class ReportParsingException extends RuntimeException {

    public ReportParsingException(String message) {
        super(message);
    }
}

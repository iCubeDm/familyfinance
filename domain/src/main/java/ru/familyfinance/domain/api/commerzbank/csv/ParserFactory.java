package ru.familyfinance.domain.api.commerzbank.csv;

/**
 * author: dmitry.yakubovsky
 * date:   12/07/16
 */
public interface ParserFactory {

    CsvParser of(Bank bank);
}

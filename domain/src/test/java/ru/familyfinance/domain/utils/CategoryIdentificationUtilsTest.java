package ru.familyfinance.domain.utils;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * author: dmitry.yakubovsky
 * date:   12/04/16
 */
public class CategoryIdentificationUtilsTest {

    @Test
    public void testGetSimilarityIndex() throws Exception {
        String test = "This string should be identified by algorithm";
        String similar = "This string should be identified by algorithm as similar string";
        String notSimilar = "This string will be not recognized by similarity flow";


        assertTrue(CategoryIdentificationUtils.getSimilarityIndex(test, similar) > 70f);
        assertTrue(CategoryIdentificationUtils.getSimilarityIndex(test, notSimilar) < 70f);
    }
}

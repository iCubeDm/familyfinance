package ru.familyfinance.domain.transaction;

import org.junit.Test;
import ru.familyfinance.domain.transaction.contract.TransactionData;
import ru.familyfinance.domain.utils.DateTime;
import stubs.TestTransactionData;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

/**
 * author: dmitry.yakubovsky
 * date:   11/04/16
 */
public class TransactionDomainTest {

    @Test
    public void testIdentifyCategory() throws Exception {
        TransactionDomain similarLess = new TransactionDomain(new TestTransactionData(
                "111",
                "this transaction may be identified by algorithm",
                DateTime.parse("2015-08-08"),
                1L)
        );
        TransactionDomain similar = new TransactionDomain(new TestTransactionData(
                "111",
                "this transaction should be identified by algorithm",
                DateTime.parse("2015-08-08"),
                2L)
        );
        TransactionDomain sameTx = new TransactionDomain(new TestTransactionData(
                "333",
                "this transaction should be identified by algorithm",
                DateTime.parse("2015-08-08"),
                2L)
        );
        TransactionDomain notSimilar = new TransactionDomain(new TestTransactionData(
                "222",
                "current object definitely will not be recognized through logic flow",
                DateTime.parse("2015-08-08"),
                3L)
        );

        TransactionData testTxData = new TestTransactionData("333", "this transaction should be identified by algorithm", DateTime.parse("2015-08-08"), 0L);
        TransactionDomain testTransaction = new TransactionDomain(testTxData);

        testTransaction.identifyCategory(Arrays.asList(similarLess, similar, notSimilar, sameTx));

        assertEquals(testTransaction.getCategoryId(), similar.getCategoryId());

    }
}

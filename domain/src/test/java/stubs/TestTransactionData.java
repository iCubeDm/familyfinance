package stubs;

import ru.familyfinance.domain.transaction.contract.TransactionData;
import ru.familyfinance.domain.utils.DateTime;
import ru.familyfinance.domain.utils.Money;

/**
 * author: dmitry.yakubovsky
 * date:   11/04/16
 */
public class TestTransactionData implements TransactionData {

    private String id;
    private String description;
    private DateTime executionDate;
    private Long categoryId;

    public TestTransactionData(String id, String description, DateTime executionDate, Long categoryId) {
        this.id = id;
        this.description = description;
        this.executionDate = executionDate;
        this.categoryId = categoryId;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public Money getAmount() {
        return Money.valueOf("11");
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public DateTime getExecutionDate() {
        return executionDate;
    }

    @Override
    public Long getCategoryId() {
        return categoryId;
    }

    @Override
    public Boolean getActive() {
        return true;
    }

    @Override
    public Boolean getCategoryApproved() {
        return false;
    }
}

package ru.familyfinance.integration.commerzbank;

import ru.familyfinance.domain.transaction.contract.TransactionData;
import ru.familyfinance.domain.utils.DateTime;
import ru.familyfinance.domain.utils.Money;

/**
 * author: dmitry.yakubovsky
 * date:   11/04/16
 */
class CommerzbankTransaction implements TransactionData {


    private Money amount;
    private String description;
    private DateTime executionDate;

    public CommerzbankTransaction(CommerzbankCsvRecord record) {

        amount = record.getAmount();
        description = record.getBookingText();
        executionDate = record.getTransactionDate();
    }

    @Override
    public String getId() {
        return null;
    }

    @Override
    public Money getAmount() {
        return amount;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public DateTime getExecutionDate() {
        return executionDate;
    }

    @Override
    public Long getCategoryId() {
        return 0L;
    }

    @Override
    public Boolean getActive() {
        return true;
    }

    @Override
    public Boolean getCategoryApproved() {
        return false;
    }
}

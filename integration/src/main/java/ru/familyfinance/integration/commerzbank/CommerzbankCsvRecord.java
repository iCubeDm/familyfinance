package ru.familyfinance.integration.commerzbank;


import ru.familyfinance.domain.utils.DateTime;
import ru.familyfinance.domain.utils.Money;

/**
 * Created by dmitry.yakubovsky on 16/11/15.
 */
class CommerzbankCsvRecord {

    private final String FORMAT = "dd.MM.yyyy";

    private DateTime transactionDate;
    private String bookingText;
    private Money amount;
    private String currency;

    public CommerzbankCsvRecord(final String transactionDate,
                                final String bookingText,
                                final String amount,
                                final String currency
    ) {
        this.transactionDate = DateTime.parse(transactionDate, FORMAT);
        this.bookingText = bookingText;
        this.amount = Money.valueOf(amount);
        this.currency = currency;
    }

    public DateTime getTransactionDate() {
        return transactionDate;
    }

    public String getBookingText() {
        return bookingText;
    }

    public Money getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }

}


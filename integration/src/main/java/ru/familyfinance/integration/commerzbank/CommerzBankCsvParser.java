package ru.familyfinance.integration.commerzbank;


import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.stereotype.Component;
import ru.familyfinance.domain.api.commerzbank.csv.CsvParser;
import ru.familyfinance.domain.api.commerzbank.csv.exception.ReportParsingException;
import ru.familyfinance.domain.transaction.contract.TransactionData;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * author: dmitry.yakubovsky
 * date:   18/11/15
 */
@Component
public class CommerzBankCsvParser implements CsvParser {

    public List<TransactionData> parseCsvInputStream(InputStreamReader streamReader) {

        try {

            //CSVParser parser = new CSVParser(streamReader, CSVFormat.newFormat(';'));
            CSVParser parser = null;
            parser = new CSVParser(streamReader, CSVFormat.EXCEL);
            List<CSVRecord> list = parser.getRecords();
            list.remove(0);
            List<CommerzbankCsvRecord> rawReport = new ArrayList<>();

            for (CSVRecord csvRecord : list) {

                final String transactionDate = csvRecord.get(0);
                final String bookingText = csvRecord.get(3);
                final String amount = csvRecord.get(4);
                final String currency = csvRecord.get(5);
                CommerzbankCsvRecord record = new CommerzbankCsvRecord(transactionDate, bookingText, amount, currency);

                rawReport.add(record);
            }

            return rawReport.stream().map(CommerzbankTransaction::new).collect(Collectors.toList());

        } catch (IOException e) {
            throw new ReportParsingException("Error during report parsing: " + e.getMessage());
        }

    }
}

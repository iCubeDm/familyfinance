package ru.familyfinance.integration;

import org.springframework.stereotype.Component;
import ru.familyfinance.domain.api.commerzbank.csv.Bank;
import ru.familyfinance.domain.api.commerzbank.csv.CsvParser;
import ru.familyfinance.domain.api.commerzbank.csv.ParserFactory;
import ru.familyfinance.integration.commerzbank.CommerzBankCsvParser;
import ru.familyfinance.integration.number26.Number26CsvParser;

/**
 * author: dmitry.yakubovsky
 * date:   12/07/16
 */
@Component
public class CsvParserFactory implements ParserFactory {

    @Override
    public CsvParser of(Bank bank) {
        switch (bank) {
            case COMMERZBANK:
                return commerzbank();
            case NUMBER26:
                return number26();
            default:
                throw new IllegalArgumentException("Unknown bank");
        }
    }

    private static CsvParser number26() {
        return new Number26CsvParser();
    }

    private static CsvParser commerzbank() {
        return new CommerzBankCsvParser();
    }
}

package ru.familyfinance.integration.number26;

import ru.familyfinance.domain.utils.DateTime;
import ru.familyfinance.domain.utils.Money;

/**
 * author: dmitry.yakubovsky
 * date:   12/07/16
 */
class Number26CsvRecord {

    private DateTime transactionDate;
    private String bookingText;
    private Money amount;
    private String currency;

    public Number26CsvRecord(final String transactionDate,
                             final String bookingText,
                             final String amount,
                             final String currency
    ) {
        this.transactionDate = DateTime.parse(transactionDate);
        this.bookingText = bookingText;
        this.amount = Money.valueOf(amount);
        this.currency = currency;
    }

    public DateTime getTransactionDate() {
        return transactionDate;
    }

    public String getBookingText() {
        return bookingText;
    }

    public Money getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }
}

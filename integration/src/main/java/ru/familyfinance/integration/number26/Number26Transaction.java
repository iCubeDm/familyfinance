package ru.familyfinance.integration.number26;

import ru.familyfinance.domain.transaction.contract.TransactionData;
import ru.familyfinance.domain.utils.DateTime;
import ru.familyfinance.domain.utils.Money;

/**
 * author: dmitry.yakubovsky
 * date:   12/07/16
 */
class Number26Transaction implements TransactionData {

    private final Money amount;

    private final String description;

    private final DateTime executionDate;

    public Number26Transaction(Number26CsvRecord record) {
        this.amount = record.getAmount();
        this.description = record.getBookingText();
        this.executionDate = record.getTransactionDate();
    }

    @Override
    public String getId() {
        return null;
    }

    @Override
    public Money getAmount() {
        return amount;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public DateTime getExecutionDate() {
        return executionDate;
    }

    @Override
    public Long getCategoryId() {
        return 0L;
    }

    @Override
    public Boolean getActive() {
        return true;
    }

    @Override
    public Boolean getCategoryApproved() {
        return false;
    }
}

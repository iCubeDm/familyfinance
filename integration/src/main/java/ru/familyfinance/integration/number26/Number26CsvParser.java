package ru.familyfinance.integration.number26;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import ru.familyfinance.domain.api.commerzbank.csv.CsvParser;
import ru.familyfinance.domain.api.commerzbank.csv.exception.ReportParsingException;
import ru.familyfinance.domain.transaction.contract.TransactionData;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * author: dmitry.yakubovsky
 * date:   12/07/16
 */
public class Number26CsvParser implements CsvParser {

    @Override
    public List<TransactionData> parseCsvInputStream(InputStreamReader streamReader) {

        try (CSVParser parser = new CSVParser(streamReader, CSVFormat.EXCEL)) {

            //CSVParser parser = new CSVParser(streamReader, CSVFormat.newFormat(';'));

            List<CSVRecord> list = parser.getRecords();
            list.remove(0);
            List<Number26CsvRecord> rawReport = new ArrayList<>();

            for (CSVRecord csvRecord : list) {

                final String transactionDate = csvRecord.get(0);
                final String bookingText = csvRecord.get(1) + " " + csvRecord.get(4);
                final String amount = csvRecord.get(6);
                final String currency = "EUR";
                Number26CsvRecord record = new Number26CsvRecord(transactionDate, bookingText, amount, currency);

                rawReport.add(record);
            }

            return rawReport.stream().map(Number26Transaction::new).collect(Collectors.toList());

        } catch (IOException e) {
            throw new ReportParsingException("Error during report parsing: " + e.getMessage());
        }

    }
}


package ru.familyfinance.integration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * author: dmitry.yakubovsky
 * date:   11/04/16
 */
@Configuration
@ComponentScan({
        "ru.familyfinance.integration"
})
@PropertySource("classpath:integration.properties")
public class IntegrationConfig {
}

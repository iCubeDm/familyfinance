package ru.familyfinance.integration.commerzbank;

import org.junit.Test;
import ru.familyfinance.domain.utils.DateTime;
import ru.familyfinance.domain.utils.Money;

import static org.junit.Assert.*;

/**
 * author: dmitry.yakubovsky
 * date:   12/07/16
 */
public class CommerzbankCsvRecordTest {

    @Test
    public void testCreateRecord() {
        String amount = "10.00";
        String currency = "EUR";
        String date = "20.08.1991";
        String bookingText = "TEST";

        CommerzbankCsvRecord record = new CommerzbankCsvRecord(date, bookingText, amount, currency);

        assertEquals(Money.valueOf(amount), record.getAmount());
        assertEquals(currency, record.getCurrency());
        assertEquals(DateTime.parse("1991-08-20"), record.getTransactionDate());
        assertEquals(bookingText, record.getBookingText());
    }
}

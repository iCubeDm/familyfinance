package ru.familyfinance.persistence.springdata;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.familyfinance.domain.transaction.contract.TransactionData;
import ru.familyfinance.persistence.entity.TransactionEntity;

import java.util.List;

/**
 * author: dmitry.yakubovsky
 * date:   09/04/16
 */
public interface TransactionRepositoryJpa extends JpaRepository<TransactionEntity, String> {

    List<TransactionEntity> findAllByCategoryApprovedTrueAndActiveTrue();

    List<TransactionEntity> findAllByCategoryApprovedFalseAndActiveTrue();

    List<TransactionEntity> findAllByExecutionDateBetweenAndActiveTrue(String from, String to);

    List<TransactionData> findAllByCategoryId(Long id);
}

package ru.familyfinance.persistence.springdata;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import ru.familyfinance.persistence.entity.CategoryEntity;

/**
 * author: dmitry.yakubovsky
 * date:   11/04/16
 */
public interface CategoryRepositoryJpa extends JpaRepository<CategoryEntity, Long> {
}

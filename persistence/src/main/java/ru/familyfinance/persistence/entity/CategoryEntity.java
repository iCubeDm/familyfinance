package ru.familyfinance.persistence.entity;

import ru.familyfinance.domain.category.contract.CategoryData;

import javax.persistence.*;

/**
 * ru.familyfinance.persistence.entity
 * Created by iCubeDm on 09.04.2016.
 */
@Entity
@Table(name = "categories")
public class CategoryEntity implements CategoryData {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String name;

    public CategoryEntity() {
    }

    public CategoryEntity(CategoryData data) {
        id = data.getId();
        name = data.getName();
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }
}

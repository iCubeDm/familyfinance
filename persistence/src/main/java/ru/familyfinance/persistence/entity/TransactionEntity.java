package ru.familyfinance.persistence.entity;

import com.google.common.base.Objects;
import org.apache.commons.lang3.StringUtils;
import ru.familyfinance.domain.identity.Identifier;
import ru.familyfinance.domain.transaction.contract.TransactionData;
import ru.familyfinance.domain.utils.DateTime;
import ru.familyfinance.domain.utils.Money;
import ru.familyfinance.persistence.utils.JsonUtils;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.List;

/**
 * author: dmitry.yakubovsky
 * date:   09/04/16
 */
@Entity
@Table(name = "transaction")
public class TransactionEntity implements TransactionData, Serializable {

    @Id
    private String id;

    @Column
    private String amount;

    @Column(length = 1024)
    private String description;

    @Column
    private String executionDate;

    @Column
    private Boolean active;

    @Column
    private Boolean categoryApproved;

    @Column
    private Long categoryId = 0L;

    TransactionEntity() {
    }

    public TransactionEntity(TransactionData data) {

        this.amount = data.getAmount().getAmount();
        this.description = data.getDescription();
        this.active = data.getActive();
        this.executionDate = data.getExecutionDate().iso8601();
        this.categoryApproved = data.getCategoryApproved();
        this.categoryId = data.getCategoryId();

        if (StringUtils.isBlank(data.getId()))
            this.id = buildId(data);
        else
            this.id = data.getId();
    }


    public void deactivate() {
        id = id + "_deactivated_" + DateTime.now().iso8601();
        active = false;
    }

    public String getId() {
        return id;
    }

    public Money getAmount() {
        return Money.valueOf(amount);
    }

    public String getDescription() {
        return description;
    }

    public DateTime getExecutionDate() {
        return DateTime.parse(executionDate);
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public Boolean getActive() {
        return active;
    }

    public Boolean getCategoryApproved() {
        return categoryApproved;
    }

    public boolean containsInList(List<TransactionData> transactionsList) {
        return transactionsList.stream().filter(tx -> tx.getId().equals(this.getId())).count() > 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TransactionEntity that = (TransactionEntity) o;
        return Objects.equal(getAmount(), that.getAmount()) &&
                Objects.equal(getDescription(), that.getDescription()) &&
                Objects.equal(getExecutionDate(), that.getExecutionDate()) &&
                Objects.equal(getActive(), that.getActive());
    }

    public String buildId(TransactionData data) {

        final String str2hash = String.format("%s%s%s%s",
                data.getAmount().getAmount(),
                data.getDescription(),
                data.getExecutionDate().iso8601(),
                data.getActive().toString()
        );

        return Identifier.of(str2hash).get();
    }

    public TransactionData toInterface() {
        return this;
    }

    @Override
    public String toString() {
        return JsonUtils.toJson(this);
    }
}

package ru.familyfinance.persistence.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.familyfinance.domain.category.contract.CategoryData;
import ru.familyfinance.domain.repository.TransactionRepository;
import ru.familyfinance.domain.transaction.contract.TransactionData;
import ru.familyfinance.domain.utils.DateTime;
import ru.familyfinance.persistence.entity.TransactionEntity;
import ru.familyfinance.persistence.springdata.TransactionRepositoryJpa;

import java.util.List;
import java.util.stream.Collectors;

/**
 * author: dmitry.yakubovsky
 * date:   09/04/16
 */
@Component
public class TransactionRepositoryPersistence implements TransactionRepository {

    private final static Logger logger = LoggerFactory.getLogger(TransactionRepositoryPersistence.class);

    @Autowired
    private TransactionRepositoryJpa transactionRepository;

    @Override
    public void saveOrUpdate(List<TransactionData> transactions) {

        logger.info("Requested saveOrUpdate TX collection -> {}", transactions);

        List<TransactionEntity> txEntities = transactions.stream().map(TransactionEntity::new).collect(Collectors.toList());

        logger.info("Saving transactions -> {}", txEntities);
        transactionRepository.save(txEntities);
    }

    @Override
    public void saveUnique(List<TransactionData> transactions) {

        logger.info("Requested saveUnique TX collection -> {}", transactions);

        List<TransactionEntity> txEntities = transactions.stream().map(TransactionEntity::new).collect(Collectors.toList());

        List<TransactionData> categorizedTransactions = this.findAllCategorizedTransactions();

        List<TransactionEntity> transactionsForSave = txEntities.stream()
                .filter(entity -> !entity.containsInList(categorizedTransactions))
                .collect(Collectors.toList());


        logger.info("Saving transactions -> {}", transactionsForSave);
        transactionRepository.save(transactionsForSave);
    }

    @Override
    public void saveOrUpdate(TransactionData transaction) {
        logger.info("Requested saveOrUpdate TX -> {}", transaction);

        TransactionEntity entity = new TransactionEntity(transaction);

        logger.info("Saving transaction -> {}", entity);
        transactionRepository.save(entity);
    }

    @Override
    public List<TransactionData> fetchAllByInterval(DateTime from, DateTime to) {

        List<TransactionEntity> entities = transactionRepository.findAllByExecutionDateBetweenAndActiveTrue(from.iso8601(), to.iso8601());

        return entities.stream().map(TransactionEntity::toInterface).collect(Collectors.toList());
    }

    @Override
    public List<TransactionData> findAllCategorizedTransactions() {

        final List<TransactionEntity> entities = transactionRepository.findAllByCategoryApprovedTrueAndActiveTrue();

        return entities.stream().map(TransactionEntity::toInterface).collect(Collectors.toList());
    }

    @Override
    public List<TransactionData> findAllUncategorizedTransactions() {

        final List<TransactionEntity> entities = transactionRepository.findAllByCategoryApprovedFalseAndActiveTrue();

        return entities.stream().map(TransactionEntity::toInterface).collect(Collectors.toList());
    }

    @Override
    public void createTransaction(TransactionData transactionData) {

        TransactionEntity entity = new TransactionEntity(transactionData);

        logger.info("Saving transaction -> {}", entity);
        transactionRepository.save(entity);
    }

    @Override
    public void deactivateTransaction(String transactionId) {

        TransactionEntity entity = transactionRepository.findOne(transactionId);

        if (entity == null) {
            logger.info("Transaction with ID {} was not found", transactionId);
            return;
        }

        transactionRepository.delete(entity);

        entity.deactivate();

        logger.info("Deactivating transaction -> {}", entity);
        transactionRepository.save(entity);
    }

    @Override
    public List<TransactionData> findAllTransactionsWithCategory(CategoryData category) {
        return transactionRepository.findAllByCategoryId(category.getId());
    }
}

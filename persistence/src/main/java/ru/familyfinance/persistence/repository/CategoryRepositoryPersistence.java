package ru.familyfinance.persistence.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.familyfinance.domain.category.contract.CategoryData;
import ru.familyfinance.domain.repository.CategoryRepository;
import ru.familyfinance.persistence.entity.CategoryEntity;
import ru.familyfinance.persistence.springdata.CategoryRepositoryJpa;
import ru.familyfinance.persistence.utils.JsonUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * author: dmitry.yakubovsky
 * date:   11/04/16
 */
@Component
public class CategoryRepositoryPersistence implements CategoryRepository {

    private final static Logger logger = LoggerFactory.getLogger(CategoryRepositoryPersistence.class);

    @Autowired
    private CategoryRepositoryJpa categoryRepositoryJpa;

    @Override
    public List<CategoryData> findAll() {

        final Iterable<CategoryEntity> entities = categoryRepositoryJpa.findAll();

        List<CategoryData> converted = new ArrayList<>();
        entities.forEach(converted::add);

        return converted;
    }

    public void saveCategory(CategoryData category) {

        CategoryEntity entity = new CategoryEntity(category);

        logger.info("Saving category -> {}", JsonUtils.toJson(entity));
        categoryRepositoryJpa.save(entity);
    }

    @Override
    public void deleteCategory(CategoryData category) {
        CategoryEntity entity = new CategoryEntity(category);

        logger.info("Deleting category -> {}", JsonUtils.toJson(entity));
        categoryRepositoryJpa.delete(entity);
    }

    @Override
    public CategoryData findById(Long categoryId) {
        return categoryRepositoryJpa.findOne(categoryId);
    }
}

package ru.familyfinance.persistence;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * author: dmitry.yakubovsky
 * date:   19/02/16
 */
@Configuration
@ComponentScan({"ru.familyfinance.persistence.repository"})
@Import({MemoryDataSourceConfig.class, PersistentDataSourceConfig.class, JpaConfig.class})
public class PersistenceConfig {

}
